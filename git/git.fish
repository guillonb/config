#!/usr/bin/fish

set -l mode
if set -q argv[1]
	set mode "$argv[1]"
	set -e argv[1]
else
	__error "Mode expected"
	exit 1
end

switch "$mode"
	case "ignore"
		set -q argv[1]
		and echo $argv >>(git root)/.gitignore
		and exit 0
		test -f (git root)/.gitignore
		and echo (git root)/.gitignore
	case "pwd"
		pwd
	case "tree"
		argparse -n git-tree -i 'u/untracked' -- $argv
		if set -q _flag_u
			argparse -n git-tree -i 'I=' -- $argv
			command tree --prune -I (string join '|' (cat (git root)/.gitignore | sed 's/,/|/g'; echo $_flag_I)) $argv
		else
			command git ls-files -z | string split0 | tree --fromfile . $argv
		end
	case "url"
		set -q argv[1]
		or set argv[1] "origin"
		git remote get-url $argv[1] | sed -e 's|^git@\(.*\)\.git$|https://\1|g'
	case "*"
		__error -- "Unknwown mode $mode"
end

