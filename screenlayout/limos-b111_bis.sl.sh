#!/bin/sh
xrandr \
	--output VIRTUAL1 --off \
	--output DP1 --mode 1280x1024 --pos 0x0 --scale 2.5x2.5 --rotate left \
	--output eDP1 --primary --mode 3840x2160 --pos 2560x1040 --rotate normal \
	--output DP2 --off
