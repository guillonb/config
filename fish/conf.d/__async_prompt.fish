status is-interactive
or exit 0

set -l xdg_runtime_dir "$XDG_RUNTIME_DIR"
test -z "$xdg_runtime_dir"
and set xdg_runtime_dir /tmp

set -g __async_prompt_tmpdir "$xdg_runtime_dir/fish-async-prompt"
mkdir -p $__async_prompt_tmpdir

# Setup after the user defined prompt functions are loaded.
function __async_prompt_setup_on_startup --on-event fish_prompt
    functions -e (status current-function)

    for func in (__async_prompt_config_functions)
        function $func -V func
            cat $__async_prompt_tmpdir'/'$fish_pid'_'$func.out 2>/dev/null
        end
    end
end

function __async_prompt_fire --on-event fish_prompt
    set _preserved_pipestatus $pipestatus
    set st $_preserved_pipestatus[-1]

    for func in (__async_prompt_config_functions)
        set -l tmpfile $__async_prompt_tmpdir'/'$fish_pid'_'$func

        if functions -q $func'_loading_indicator'
            cat $tmpfile'.out' 2>/dev/null | read -zl last_prompt
            eval (string escape -- $func'_loading_indicator' "$last_prompt") >$tmpfile'.out'
        end

        __async_prompt_config_inherit_variables \
            | __async_prompt_spawn $tmpfile'.pid' $st \
            $func' 2>'$tmpfile'.err | read -z prompt; echo -n $prompt >'$tmpfile'.out'
    end
end

function __async_prompt_spawn
    #kill previous instance of asynchronous function (if any running)
    test -e $argv[1]
    and kill -s SIGINT (cat $argv[1]) 2>/dev/null
    set -l envs
    begin
        set st $argv[2]
        while read line
            switch "$line"
                case FISH_VERSION PWD _ history 'fish_*' hostname version
                case status
                    echo status $st
                case pipestatus
                    echo pipestatus $_preserved_pipestatus
                case SHLVL
                    set envs $envs SHLVL=(math $SHLVL - 1)
                case '*'
                    echo $line (string escape -- $$line)
            end
        end
    end | read -lz vars
    echo $vars | env $envs fish -c '
    echo $fish_pid >'$argv[1]'
    function __async_prompt_set_status
        return $argv
    end
    function __async_prompt_signal
        kill -s "'(__async_prompt_config_internal_signal)'" '$fish_pid'
    end
    while read -a line
        test -z "$line"
        and continue

        if test "$line[1]" = status
            set st $line[2]
        else if test "$line[1]" = pipestatus
            set _preserved_pipestatus $line[2]
        else
            eval set "$line"
        end
    end

    not set -q st
    and set -q _preserved_pipestatus
    and set st $_preserved_pipestatus[-1]

    not set -q st
    or __async_prompt_set_status $st
    '$argv[3]'
    __async_prompt_signal
    test -e '$argv[1]'
    and rm '$argv[1] &
    disown
end

function __async_prompt_config_inherit_variables
    if set -q async_prompt_inherit_variables
        if test "$async_prompt_inherit_variables" = all
            set -ng
        else
            string join \n $async_prompt_inherit_variables
        end
    else
        echo status
        echo SHLVL
        echo CMD_DURATION
    end
end

function __async_prompt_config_functions
    set -l funcs (
        if set -q $async_prompt_functions
            string join \n $async_prompt_functions
        else
            echo fish_prompt
            echo fish_right_prompt
        end
    )
    for func in $funcs
        functions -q "$func"
        or continue

        echo $func
    end
end

function __async_prompt_config_internal_signal
    if test -z "$async_prompt_signal_number"
        echo SIGUSR1
    else
        echo "$async_prompt_signal_number"
    end
end

function __async_prompt_repaint_prompt --on-signal (__async_prompt_config_internal_signal)
    commandline -f repaint >/dev/null 2>/dev/null
end
