for d in variables triggers
	test -d $__fish_config_dir/$d
	or continue
	for f in $__fish_config_dir/$d/*.fish
		source $f
	end
end
