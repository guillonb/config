status is-interactive
or exit 0

function next_prompt_mode -S --description "change prompt mode on empty command" --on-event fish_postexec
	switch $argv
		case ""
			set _prompt_mode (math $_prompt_mode + 1)
		case (status current-function)
		case '*'
			set -g _prompt_mode 1
			set -l i 0
			for mode in $__active_prompt_modes
				set i (math $i + 1)
				__fish_check_prompt_mode $i
			end
	end
end

function __fish_check_prompt_mode -S -a midx
	set -l mode $__active_prompt_modes[$midx]
	switch $mode
		case ".*" "×*" "@*"
			:
		case "-*" "+*"
			set -l mode (string sub -s 3 H$mode)
			switch $mode
				case "git"
					command git root 2>/dev/null >&2
				case "hg"
					command hg root 2>/dev/null >&2
				case "svn"
					command svnadmin info 2>/dev/null >&2
				case "*"
					not functions -q __fish_check_right_prompt_mode_$mode
					or eval __fish_check_right_prompt_mode_$mode
			end
			and set __active_prompt_modes[$midx] +$mode
			or set __active_prompt_modes[$midx] -$mode
		case "*"
			if functions -q __fish_check_right_prompt_mode_$mode
				eval __fish_check_right_prompt_mode_$mode
				and set __active_prompt_modes[$midx] +$mode
				or set __active_prompt_modes[$midx] -$mode
			else
				set __active_prompt_modes[$midx] @$mode
			end
	end
end
