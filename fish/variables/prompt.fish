set -x _prompt_resetcolor (set_color normal)
set -x _prompt_statuscolor (set_color -d green) (set_color -d red)
set -x _prompt_pipestatuscolor (set_color -d green) (set_color -d red)
set -x _prompt_usercolor (set_color brown)
set -x _prompt_hostcolor (set_color blue) (set_color -o cyan)
set -x _prompt_pathcolor (set_color white)
set -x _prompt_repocolor (set_color -d blue)
set -x _prompt_timecolor (set_color -doi blue)
set -x _prompt_returncolor (set_color -doi white)
set -x _prompt_lockcolor (set_color green) (set_color red)
set -x _prompt_unknownmodecolor (set_color -o red)
set -x _prompt_oldcolor (set_color brblack)

#prompt mode management
#__active_prompt_modes: array of modes, prefixed by [-+] (de)activables, [.×] hidden (always unactive), or [@] always active
#  A mode MODE not prefixed by any char in [-+.×@] are automatically prepended with [-+@] according to whether __fish_check_right_prompt_mode_MODE exists or not (@), and returns 0 [+] or not [-].
set -gx -- __active_prompt_modes "-git" "-hg" "-svn" "@time" "@return" "@locks"
set -gx -- _preserved_pipestatus
set -gx -- _prompt_mode 1
#asynchronous prompt functions
set -a async_prompt_functions fish_left_prompt fish_right_prompt
set -a async_prompt_inherit_variables __active_prompt_modes _prompt_mode _preserved_pipestatus CMD_DURATION

#git prompt
set -x __fish_git_prompt_showdirtystate
#set -x __fish_git_prompt_showuntrackedfiles
set -x __fish_git_prompt_show_informative_status
set -x __fish_git_prompt_showupstream
set -x __fish_git_prompt_showcolorhints
set -x __fish_git_prompt_shorten_branch_char_suffix
#set -x __fish_git_prompt_showstashstate

#hg prompt
set -x fish_prompt_hg_show_informative_status

