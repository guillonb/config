
#lbin dir
for lbindir in "$HOME/prgm/bin" "$HOME/.local/bin"
	test -d $lbindir
	or continue
	contains -- $lbin $PATH
	or contains -- $lbin fish_user_path
	#or set -Up fish_user_path "$lbindir"
	or set -gpx PATH "$lbindir"
end

#sbin dirs
for sbindir in "/sbin" "/usr/sbin" "/usr/local/sbin"
	test -d "$sbindir"
	or continue
	contains -- "$sbindir" $PATH
	or set -gpx PATH "$sbindir"
end

#function path
#set -gp fish_function_path $HOME/.config/fish/wrappers
set -gp fish_function_path $HOME/.config/fish/functions/links
