set -q MUSTACHE_PARTIALS_PATH
or set -g MUSTACHE_PARTIALS_PATH

for d in "./mustache" "~/documents/work/teaching/tools/moodle/mustache"
	contains "$d" $MUSTACHE_PARTIALS_PATH
	or set -ga MUSTACHE_PARTIALS_PATH "$d"
end
