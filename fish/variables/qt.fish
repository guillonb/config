# according to https://wiki.archlinux.org/index.php/HiDPI#X_Server
# and: https://doc.qt.io/qt-5/highdpi.html
export QT_AUTO_SCREEN_SCALE_FACTOR=1
export QT_ENABLE_HIGHDPI_SCALING=1
#export QT_SCALE_FACTOR_ROUNDING_POLICY
export QT_SCALE_FACTOR=1.15
#export QT_SCREEN_SCALE_FACTORS
export QT_FONT_DPI=192 vym
