#variables for fish_messages.fish
set -x -- Com_prefix "Msg: "
set -x -- Dbg_prefix "Dbg: "
set -x -- Err_prefix "Err: "
set -x -- War_prefix "War: "
set -x -- Val_prefix "Ok: "
set -x -- Vrb_prefix ""

set -x -- Com_suffix "."
set -x -- Dbg_suffix "."
set -x -- Err_suffix "!"
set -x -- War_suffix "."
set -x -- Val_suffix "."
set -x -- Vrb_suffix ""

set -x -- Com_color "white"
set -x -- Dbg_color "-b blue -o white"
set -x -- Err_color "-o red"
set -x -- War_color "yellow"
set -x -- Val_color "-o 0F0"
set -x -- Vrb_color "normal"

set -x -- Com_level 4
set -x -- Dbg_level 5
set -x -- Err_level 1
set -x -- War_level 2
set -x -- Val_level 2
set -x -- Vrb_level 3

set -x -- Com_fd '&1'
set -x -- Dbg_fd '&2'
set -x -- Err_fd '&2'
set -x -- War_fd '&2'
set -x -- Val_fd '&1'
set -x -- Vrb_fd '&1'

