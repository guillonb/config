set -gx -- TEXMFHOME $HOME/.texmf
set -gpx --path -- TEXINPUTS .:cls:sty:tikz:tex:
set -gpx --path -- BIBINPUTS .:bib:
set -gpx --path -- BSTINPUTS .:bst:

