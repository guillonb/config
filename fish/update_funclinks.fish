#!/usr/bin/fish
set -l confd $HOME/.config/fish
mkdir -p $confd/functions/links
for fff in $confd/functions/*.fish;
	for fan in (grep -e '^\(and \)\?alias' $fff | sed -e 's/^\(and \|or \)\?alias\s\+\([^\t ]*\)\s.*$/\2/g')
		test $fff = $fan.fish
		or ln -frs $fff $confd/functions/links/$fan.fish
	end
	for ffn in (grep -e '^\(and \|or \)function' $fff | sed -e 's/^\(and \|or \)\?function\s\+\([^\t ]*\)\(\s.*\)\?$/\2/g')
		test $fff = $ffn.fish
		or ln -frs $fff $confd/functions/links/$ffn.fish
	end
end
