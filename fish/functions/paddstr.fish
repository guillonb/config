function paddstr
	argparse -i 'h/help' -- $argv
	set -q _flag_h
	and echo \
'Usage: paddstr [-p STRING] [-l N] [-t] [LINE]

Padds the line given as argument or, if missing, padds each line read from standard input.

Options:
-l --length N    padd the line up to length N (default to terminal line width)
-p --padd-with STRING
                 use STRING for padding the lines (default is the single space string)
-s --sep STRING  append STRING to each line of length less than N before padding it
-t               truncate line which are longer than N

Argument:
LINE
	if provided, the line LINE to padd, otherwise each line read from standard input is padded.
'
	and return 0

	argparse 'p/padd-with=' 'l#length' 's/sep=' 't/truncate' -- $argv

	set -q _flag_l
	and set -l size $_flag_l
	or set -l size (tput cols)
	set -q _flag_p
	and set -l char $_flag_p
	or set -l char " "

	set -l charlength (echo -- $char | wc -L)

	if set -q argv[1]
		echo $argv | paddstr -p $char -l $size $_flag_t
	else
		while read line
			set -l length (echo -- $line | wc -L)
			test $length -lt $size
			and set line "$line$_flag_s"
			set -l length (echo -- $line | wc -L)
			if test $length -ge $size
				set -q _flag_t
				and builtin string sub -l $size $line
				or echo $line
			else
				set -l padd (builtin string repeat -n (math "($size - $length)" / $charlength | cut -d'.' -f1) -- $char)
				echo $line$padd
			end
		end
	end
end
