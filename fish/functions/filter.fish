function filter --description 'basic filter that test whether mandatory and forbidden words are in array'

	argparse --name filter -x 'h,a' -x 'h,m,l' -x 'h,f' -x 'h,N,L,X' -x 'h,M,X' -x 'h,c,s' -x 'h,i' \
		'a/any=+' 'm/mandatory=+' 'f/forbidden=+' 'l/last' \
		'L/at-least=' 'M/at-most=' 'X/exactly=' 'N/nonempty' \
		'c/commandline' 's/stdin' 'i/ignore-empty-lines' \
		'h/help' \
		-- $argv

	set -q _flag_h
	and echo "filter [-m <string>[,<string>…]] [-f <string>[,<string>]] [-c|-s|<string> [<string>…]]"
	and echo "   --mandatory|-m		specifies a list of mandatory terms (as a comma-separated list, or by repeating the option)."
	and echo "   --forbidden|-f		specifies a list of forbidden terms (as a comma-separated list, or by repeating the option)."
	and echo "   --commandline|-c   consider the array of complete commandline argument (useful for completion definition)."
	and echo "   --stdin|-s         reads array from input (default, if no positional arguments are specified)"
	and echo "<<<<and other>>>>>"
	and echo "filter --help"
	and echo "       display this help and return"
	and return 0

	set -l any (builtin string split , -- $_flag_a)
	set -l mandatory (builtin string split , -- $_flag_m)
	set -l forbidden (builtin string split , -- $_flag_f)

	if set -q _flag_c
		set -p argv (commandline -poc)
	else
		set -q _flag_s
		and set -q argv[1]
		and return 1
		
		set -q argv[1]
		or set argv[1] '-'
	end

	set -q any[1]
	and set -l found_one false
	or set -l found_one true

	switch "$argv[1]"
		case '-' #stdin mode
			set -l count 0
			set -l last_allowed true
			while read -la line

				not set -q _flag_i
				or test -n "$line" 
				and set count (math $count + 1)
				and set -e _flag_N

				if set -q _flag_l
					for a in $any
						test "$a" = "$line"
						and set found_one true
						or set found_one false
					end
				else
					for a in $any
						test "$a" = "$line"
						and set found_one true
						and break
					end
				end
				for m in $mandatory
					test "$m" = "$line"
					and eval "set -e mandatory[(contains -i -- "$m" $mandatory)]"
				end
				if set -q _flag_l
					for f in $forbidden
						test "$f" = "$line"
						and set last_allowed false
						or set last_allowed true
					end
				else
					for f in $forbidden
						test "$f" = "$line"
						and return 1
					end
				end
			end

			not $found_one
			or set -q mandatory[1]
			or not $last_allowed
			and return 1

			#non-emptiness
			set -q _flag_N
			#lower cardinality
			or begin set -q _flag_L; and test $count -lt $_flag_L; end
			#upper cardinality
			or begin set -q _flag_M; and test $count -gt $_flag_M; end
			#exact cardinality
			or begin set -q _flag_X; and test $count -ne $_flag_X; end
			and return 2
		case '*' #array mode
			if set -q _flag_i
				for item in $argv
					test -z "$item"
					and set -e argv[(contains -i -- "$item" $argv)]
				end
			end

			if set -q _flag_l
				contains -- "$argv[-1]" $a
				or return 1
				contains -- "$argv[-1]" $f
				and return 1
			else
				for a in $any
					contains -- "$a" $argv
					and set found_one true
					and break
				end
				not $found_one
				and return 1
				for m in $mandatory
					contains -- "$m" $argv
					or return 1
				end
				for f in $forbidden
					contains -- "$f" $argv
					and return 1
				end
			end

			#non-emptiness
			set -q _flag_N
			and not set -q argv[1]
			and return 2

			#lower cardinality
			set -q _flag_L
			and test (count $argv) -lt $_flag_L
			and return 2

			#upper cardinality
			set -q _flag_M
			and test (count $argv) -gt $_flag_M
			and return 2

			#exact cardinality
			set -q _flag_X
			and test (count $argv) -ne $_flag_X
			and return 2
	end
	return 0
end

