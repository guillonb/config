function cleancode
	set -l srcfilepattern "**.{py,c,sh,ml,fish}"

	set -q argv[1]
	and set -l sources $argv
	or set -l sources $srcfilepattern

	for pyfile in (ls $sources)
		test -d $pyfile
		and set -l pyfile (builtin string trim -c/ -r $pyfile)/$srcfilepattern

		if test -f $pyfile
			fish_verbose -- -n "Cleaning $pyfile… "
			sed -e 's|\s\+$||g' -e 's|\([^ \t]\)\(\s\)\s\+|\1\2|g' -i $pyfile
			and fish_verbose -- (set_color $Val_color)' done.'
			or fish_verbose -- (set_color $Err_color)' failed.'
		end
	end
end
