function clean --description 'clean compilation files based on file extension in \$CLEANEXT variable'
	#set cleanext variable
	set -q $tmpfileext
	and set -l cleanext $tmpfileext
	or set -l cleanext aux log nav out snm toc spl bbl blg bcf run.xml maf mtc mtc0 mtc1 mtc2 mtc3 mtc4 mtc5 mtc6 mtc7 mtc8 mtc9 idx ilg ind dep tml ttml tdep o tdx tmx tgx ci cmo mli cmx upa upb class vtc fls fdb_latexmk kaux diagnose axp sta stp vrb
	#remove file with extension in CLEANEXT
	for ext in $cleanext
		for file in *.$ext
			rm -vf $file
		end
	end
	#remove editing copy of file, ending with ~
	for file in *~
		rm -vf $file
	end
	#remove broken files/links
	for file in *
		test -e "$file"
		or rm -vi "$file"
	end
	#remove _minted-* dirs (created by minted package of texlive)
	for minteddir in _minted-*
		test -d "$minteddir"
		and rm -vr "$minteddir"
	end
end
