function doc2pdf -d "convert doc or docx document to pdf (based on name extension)"
	set -l n_error 0
	for f in $argv
		switch "$f"
			case '*.doc'
				soffice --headless --convert-to pdf "$f"
				and fish_valid "'"(basename "$f" .doc)".pdf' created."
				or fish_warning "An error might have been encountered."
			case '*.docx'
				pandoc "$f" --pdf-engine=xelatex -o (basename "$f" .docx)".pdf"
				or pandoc "$f" --pdf-engine=xelatex -o (basename "$f" .docx)".pdf"
				and fish_valid "'"(basename "$f" .docx)".pdf' created."
				or fish_warning "An error might have been encountered."
			case '*'
				fish_error "I have no idea how to convert the file '$f'."
				set n_error (math $n_error + 1)
		end
	end
	return $n_error
end

