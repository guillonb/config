function short_path --description 'Print a shorter version of a path'
	argparse 'C/nocolor' -- $argv
	set -l bC (set -q _flag_nocolor; or set_color purple)
	set -l eC (set -q _flag_nocolor; or set_color grey)
	set -l nC (set -q _flag_nocolor; or set_color normal)
	if test -z "$argv"
		cat	
	else
		echo $argv
	end | sed \
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/phd/write-draw/latex|~>tex|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/phd/write-draw/tikz|~>tikz|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/phd/write-draw|~>write|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/phd/bibliography|~>bib|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/phd|~>phd|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/postdoc/Warsaw201609-201704|~>pdocW|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/postdoc/Unimi201705-.|~>pdocM|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc/work/teaching|~>teach|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents/docs.enc|~>d.enc|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?documents|~>doc|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?downloads|~>dw|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?pictures/photos|~>photos|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?prgm/bash|~>bash|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?prgm/installs|~>installs|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?prgm|~>prgm|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?videos|~>video|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?music|~>music|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?share/Dropbox|~>dropbox|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?share/owncloud|~>owncloud|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?share/public|~>public|"\
		-e "s|^$HOME/\(.config/cdpath/\)\?share|~>share|"\
		-e "s|^$HOME/.config/cdpath/|~>cd/|"\
		-e "s|^$HOME/.config|~>conf|"\
		-e "s|^$HOME|~|"\
		-e "s|/[0-9]\{4\}\(\.\?[0-9]\{2\}\)\?\(\.\?[0-9]\{2\}\)\?\(-\([0-9]\{4\}\.\?\)\?[0-9]\{2\}\(\.\?[0-9]\{2\}\)\?\)\?-\([^/]*[^-/0-9]\)|/→\6|g"\
		-e "s|/\(→\?[^-._/]\{2,9\}\)[-._][^/]\{7,\}[-._]\([^-._/]\{2,9\}\)\$|/\1…\2|"\
		-e "s|/\(→\?[^/]\{3\}\)[^/]\{7,\}[-._]\([^-._/]\{2,9\}\)\$|/\1…\2|"\
		-e "s|/\(→\?[^-._/]\{2,9\}\)[-._][^/]\{7,\}\([^/]\{4\}\)\$|/\1…\2|"\
		-e "s|/\(→\?[^/]\{3\}\)[^/]\{7,\}\([^/]\{7\}\)\$|/\1…\2|"\
		-e "s|/\(→\?[^/]\{4\}\)[^/]\{3,\}/|/\1…/|g"\
		-e "s|…/\(→\?[^/]\{4\}\)[^/]\{3,\}/|…/\1…/|g"\
		-e "s|^\([^/]*\)/\([^/]\+/\)\{7,\}\([^/]\+/\)\([^/]\+\)\$|\1/$bC⋅⋅⋅$eC/\3\4|"\
		-e "s|^\(~[^/]*\)|$bC\1$eC|g"\
		-e "s|\([…→]\)|$bC\1$eC|g"\
		-e "s|\$|$nC|g"

		#date compression
		#last dir name compression with both left and right word save
		#  with right word save
		#  with left word save
		#  without word save
		#name compression
		#  twice because done only between slashes (/.*/) to avoid last dir compression
		#long path compression
		#coloration of home shortname
		#coloration of date and name compression

		#short name has length bounded by:
		#		max(|home alias|) + 1 + 3 + 1 + 6 + 1 + 15
		#		= 8 + 1 + 3 + 1 + 6 + 1 + 15
		#		= 35
end
