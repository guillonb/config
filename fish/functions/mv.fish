function mv --wraps (which mv) --description 'a wrapper for mv (move) that allows creation of target directory with -p option'
	#TODO: add a -l option for udating links when moved
	argparse -i 'h/help' -- $argv

	if set -q _flag_h
		command mv --help
		echo
		echo "============"
		echo "additional available option from wrapper:"
		echo "  -p, --parent                     create destination directory if nonexistent"
		return 0
	end

	argparse -i 'p/parents' -- $argv

	if not set -q _flag_p;
		command mv $argv
		return $status
	end

	set -l orig_argv $argv
	argparse -N 1 'b' 'B-backup=?' 'f/force' 'i/interactive' \
		'n/no-clobber' 's-strip-trailing-slashes' 'S/suffix=' 't/target-directory=' 'T/no-target-directory' \
	   	'u/update' 'v/verbose' 'Z/context' 'h-help' 'V-version' -- $argv
	if set -q _flag_t
		set -l dest $_flag_t
		mkdir -p $dest
	else
		set -l dest $argv[-1]
		if test (count $argv) -lt 2; or test -e $dest
			:
		else if set -q _flag_T
			mkdir -p (dirname $dest)
		else if test (count $argv) -gt 2
			mkdir -p $dest
		else if test -d $argv[1]
			mkdir -p (dirname "$dest")
		else if test (builtin string sub -s -1 -l 1 -- $dest) = '/'
			mkdir -p $dest
		else #consider dest as a file
			mkdir -p (dirname $dest)
		end
	end
	command mv $orig_argv
end
