function isbinaryfile -d "return 0 if and only if the argument is a regular binary file"
	argparse -i 'h/help' -- $argv
	set -q _flag_help
	or ! count $argv >/dev/null
	and echo "Usage: "(status current-command)" [-q] [-a|-o] <file> [<files>…]"
	and echo "       "(status current-command)" -h"
	and return (set -q _flag_help; echo $status)

	_iskindfile 'binary' --kind_print='binary' $argv
end
function istextfile -d "return 0 if and only if the argument is a regular text file"
	argparse -i 'h/help' -- $argv

	set -q _flag_help
	or ! count $argv >/dev/null
	and echo "Usage: "(status current-command)" [-q] [-a|-o] <file> [<files>…]"
	and echo "       "(status current-command)" -h"
	and return (set -q _flag_help; echo $status)

	_iskindfile '\(ascii\|utf-8\)' --kind_print='text' $argv
end


function _iskindfile -d "auxiliary function for testing type of file"
	#Usage: _iskindfile [-q] [-a|-o] <kind> <file> [<files>…]
	argparse -N2 --exclusive 'a,o' 'k-kind_print=?' 'q/quiet' 'a-and' 'o-or' -- $argv

	set -l pattern $argv[1]
	set -l answers "yes"	"no"	"found"	"not found"
	set -l results 0		0		0		0

	for arg in $argv[2..-1]
		if test -f $arg
			set results[3] (math $results[3] + 1)
		   	if file --mime-encoding $arg | grep -q $pattern'$'
				set res 1
			else
				set res 2
			end
		else
			set res 4
		end
		set results[$res] (math $results[$res] + 1)
		set -q _flag_quiet
		or echo $arg: $answers[$res]
	end
	if begin ! set -q _flag_quiet; and test $results[3] -gt 1; end
		set -q _flag_kind_print
		and set -l ptrkind $_flag_kind_print
		or set -l ptrkind $pattern
		test $results[1] -gt 1
		and set plural "s"  "are"
		or set plural "" "is a"
		and echo -n "$results[1] over $results[3] file$plural[1] $plural[2] $ptrkind file$plural[1]"
		test $results[4] -gt 0
		and echo " ($results[4] not found)."
		or echo "."
	end
	set -q _flag_or
	and set -l res $results[1]
	or set -l res (math $results[2] + $results[4])
	return $res
end
