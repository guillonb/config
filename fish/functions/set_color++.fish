function set_color++ --wraps 'builtin set_color'
	#TODO: invert specific style
	argparse -i 'B/blink' 's/strikethrough' 'U/double-underline' -- $argv
	set -q _flag_B; and echo -ne "\e[5m"
	set -q _flag_s; and echo -ne "\e[9m"
	set -q _flag_U; and echo -ne "\e[21m"
	builtin set_color $argv
end

