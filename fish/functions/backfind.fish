function backfind -d 'find deepest specific object in the branch of ancestors.\
	This is usefull for finding a Makefile of a git repository directory when exploring the arborescence.'

	set -l usage \
		'Usage: backfind [--type {d|f|<file type>}] [--canonicalize] [--where] <name> [<directory>]' \
		'       backfind --help'

	argparse --name=backfind --max-args 2 "h/help" "f/canonicalize" "w/where" "t/type=" -- $argv
	or return

	set -q _flag_h
	and for line in $usage; echo $line; end
	and return 0

	set -l name "$argv[1]"
	set -l dirpath "$argv[2]"
	set -l type

	test -z "$name"
	and backfind --help
	and return 1
	
	test -z "$dirpath"
	and set dirpath "."

	set -q _flag_t
	and set type "$_flag_t"
	or set type "e"

	if test -$type "$dirpath/$name"
		set -q _flag_w
		and set name ""
		set -q _flag_f
		and readlink -f "$dirpath/$name"
		or echo "$dirpath/$name"
		return 0
	end

	test (readlink -f $dirpath) = "/"
	and echo "No" (set -q _flag_t; and echo -- "'$_flag_t'-typed") "'$name' found!" >/dev/stderr 
	and return 2

	backfind -t "$type" (set -q _flag_f; and echo -- "-f") (set -q _flag_w; and echo -- "-w") "$name" "$dirpath/.."
end

alias bf_makefile "backfind Makefile --type f"
alias backmake "make -C (backfind Makefile --type f)"

alias bf_git "backfind .git --type d"
#alias backgit "git -C (backfind .git --type d)"
