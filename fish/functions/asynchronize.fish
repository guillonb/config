function asynchronize --description "turn a function asynchrone"
	argparse -i 'h/help' -- $argv
	if set -q _flag_h
		echo 'Usage: asynchronize --help
		asynchronize [-CEmq] [-d DIR] [-p PID] [-s SIG]  FUNC

		== SYNOPSIS
		Transform the function named FUNC into an asynchronous function.  This is obtained by
		creating the following family of functions:
		+	_FUNC_core: a renamed version of FUNC (which is overwritten)
		+	_FUNC_fire: a wrapper of _FUNC_core which redirects standard input, output, and error
		  	to files in DIR.  Possibly (see -m, -p and -s options below), it initially kills a
				previous running instance of the function, and it finally sends signal SIG to the
				process of id PID.
		+ _FUNC_get: a function that outputs the content of the files in DIR corresponding to
				FUNC to standard outpute, standard error, and return the corresponding code.  This
				function is not an event handler: it is up to the user to handle SIG as intended.
		+ FUNC (overwritten): a launcher for the asynchronous _FUNC_fire.  Additionnally, the
				function _FUNC_get is initially called so that previous result, if any, is
				instantenaously returned.
		+ _FUNC_signal: a function that outputs the signal SIG to send to the process of id PID,
				when -s is provided, or returns 1 otherwise.  This function might be overwritten to
				change the signal to send.
		+	_FUNC_cleanup (if -C is not given): an event handler to be call when the process of id
				PID dies.  This function removes DIR.
		Finally, the function outputs DIR.

		Although _FUNC_core and _FUNC_fire could be left undefined in the current shell (as being
		run from a subshell), they are defined so that a user can later redefine them.

		== OPTIONS
		--no-cleanup  -C            do not create cleanup event handler.
		--directory -d DIR					put files in directory DIR.  Default is to write files in
		    $XDG_RUNTIME_DIR/$PID-asynchronics directory — if $XDG_RUNTIME_DIR is undefined,
				/tmp is used instead.
		--no-async-error  -E				do not output error in _FUNC_get.
		--help  -h									show this help and returns. 
		--multiple-instances  -m		allow multiple running instances of the asynchrone function,
				i.e., do not kill previous instance at the beginning of _FUNC_fire.
		--pid  -p PID								define the target process to be those of id PID.  PID is used
				to generate the default DIR name, and, if -s is given, as the target process of the
				signal finally emitted by _FUNC_fire.  If the option is not provided, $fish_pid is
				used as default.
		--quiet -q									do not ouput DIR path.
		--signal  -s SIG						make _FUNC_fire send the signal SIG to the process of id PID
				when _FUNC_core has returned, so that the latter knows a (new) result is available.
				This signal is handled by none of the created functions.  If -s is not provided then
				no signal is sent.
		' | sed -e 's/^\t\t//g' -e 's/\t/  /g'
		return 0
	end

	set -l origargv $argv
	argparse -N1 -X1 'd/directory=' 'm/multiple-instances' 'n/no-cleanup' 'p/pid=' 'q/quiet' 's/signal=' -- $argv
	set -l fbasename $argv[1]

	set -q _flag_m
	and set -l multinstances
	set -q _flag_E
	and set -l no_async_error

	set -q _flag_p
	and set -l fpid $_flag_p
	or set -l fpid $fish_pid

	set -q _flag_d
	and set -l sdir (eval echo $_flag_d | string escape)
	or set -l sdir (test -n "$XDG_RUNTIME_DIR"; and echo "$XDG_RUNTIME_DIR/$fpid-asynchronics"; or echo "/tmp/$fpid-asynchronics")

	set -q _flag_s
	and set -l signal $_flag_s

	not functions -q $fbasename
	and fish_error "No $fbasename function known"
	and return 1
	
	set -l fcorename "_"$fbasename"_core"
	set -l fsignalname "_"$fbasename"_signal"
	set -l ffirename "_"$fbasename"_fire"
	set -l fgetname "_"$fbasename"_get"
	set -l fcleanup "_"$fbasename"_cleanup"

	functions -c $fbasename $fcorename

	set -q signal
	and eval 'function '$fsignalname'; echo '$signal'; end'
	or function $fsignalname; return 1; end 

	function $ffirename -V sdir -V signal -V fpid -V multinstances -V fsignalname -V fbasename -V fcorename
		mkdir -p $sdir 
		# CLEAN PREVIOUS INSTANCE
		if set -q multinstances
			kill -SIGINT (cat "$sdir/$fbasename.pid" 2>/dev/null) 2>/dev/null
			rm -f $sdir/$fbasename.return
		end
		# EXPOSE PID
		echo $fish_pid >"$sdir/$fbasename.pid"
		# SET REDIRECTION FILES
		set -l fout "$sdir/$fbasename.out"
		set -l vout
		set -l ferr "$sdir/$fbasename.err"
		set -l verr
		set -l fret "$sdir/$fbasename.return"
		test -e "$sdir/$fbasename.in"
		and set -l fin "$sdir/$fbasename.in"
		or set -l fin "/dev/null"
		# EVALUATE
		begin
			eval $fcorename <$fin | read -z vout
		end 2>&1 | read -z verr
		# WRITE RESULT
		echo -n $status >"$fret"
		echo -n $vout >"$fout"
		echo -n $verr >"$ferr"
		# LET PARENT KNOW THAT FUNCTION AS ENDED
		set -q signal
		and kill -($fsignalname) $fpid
	end

	if status is-interactive
		function $fgetname -V no_async_error -V sdir -V fbasename
			cat "$sdir/$fbasename.out" >&1 2>/dev/null
			set -q no_async_error
			or cat "$sdir/$fbasename.err" >&2 2>/dev/null
			return (cat "$sdir/$fbasename.return" 2>/dev/null)
		end

		function $fbasename -V fcorename -V fsignalname -V ffirename -V fgetname -V sdir -V multinstances -V fpid -V signal
			eval $fgetname
			set -lx sdir $sdir
			set -lx multinstances $multinstances
			set -lx fpid $fpid
			set -lx signal $signal
			set -lx fcorename $fcorename
			set -lx fsignalname $fsignalname
			set -lx ffirename $ffirename
			fish (begin functions $fcorename; functions $fsignalname; functions $ffirename; echo "$ffirename"; end | psub) &
			disown
		end

		if not set -q _flag_n
			function $fcleanup --on-process-exit $fpid -V sdir -V fbasename
				test -e $sdir/$fbasename.pid
				and kill -SIGINT (cat $sdir/$fbasename.pid)
				rm -fr $sdir
			end
		end
	end

	set -q _flag_q
	or echo $sdir
end
