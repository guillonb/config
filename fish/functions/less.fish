function cless -w less -d "less wrapper that color output using pygmentize"
	command pygmentize -g $argv | command less -R
end

function xless -w less -d "less wrapper that is run in an external xterm window"
	xterm -e "command less $argv"
end

function cxless -w less -d "less wrapper combining cless and xless features"
	xterm -e "command pygmentize -g $argv | command less -R"
end
