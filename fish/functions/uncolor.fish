function uncolor
	begin
		set -q argv[1]
		and echo $argv
		or cat
	end | sed -r 's/\x1B\[[0-9;]*[JKmsu]//g'
end
