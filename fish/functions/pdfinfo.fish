function pdfgetinfo --description "get particular info from pdf `info' meta data (using `pdfinfo`)"
	set -l pdffiles
	set -l key
	set -l ret 0

	argparse --name "pdfget" --min-args 0 -x 'h,f,F' -x 'f,F' 'h/help' 'f/filename' 'F/nofilename' -- $argv

	set -q _flag_h
	and echo "pdfget [--filename|--nofilename] [t|a|s|k|p|d] <pdf file> [<pdf files>…]"
	and echo "pdfget --help"
	and return 0

	set -q _flag_f
	and set -l filename true

	set -q _flag_F
	and set -l filename false

	switch $argv[1]
		case title Title TITLE t T
			set key 'Title'
		case author Author AUTHOR a A
			set key 'Author'
		case subject Subject s S
			set key 'Subject'
		case creator Creator CREATOR c C
			set key 'Creator'
		case keywords Keywords KEYWORDS k K
			set key 'Keywords'
		case creationdate "creation date" Creationdate CreationDate "Creation date" "Creation Date" CREATIONDATE "CREATION DATE"
			set key 'CreationDate'
		case moddate "mod date" Moddate ModDate "Mod date" "Mod Date" MODDATE modificationdate Modificationdate ModificationDate "modification date" "Modification date" "Modification Date"
			set key 'ModDate'
		case pages Pages PAGES p P
			set key 'Pages'
		case date Date DATE d D
			set key "CreationDate"
		case all All ALL *
			set -e key
			set pdffiles "$argv[1]"
	end

	set -e argv[1]
	set pdffiles $pdffiles $argv

	test -z "$pdffiles"
	and fish_error "PDF file expected."
	and return 2

	#print filename only if
	#  either filename is true
	#  or filename is undefined and there are more than one pdf files
	not set -q filename
	and set -q pdffiles[2]
	and set -l filename true

	for f in $pdffiles
		set -l result

		eval $filename
		and echo -n "$f: "

		if set -q key
			if test -z "$key"
				pdfinfo "$f"
				set -e result
			else
				set result (pdfinfo "$f" | grep -e '^'$key':' | sed -e 's/^'$key':  *//g')
			end
		else
			pdfinfo "$f"
			set -e result
		end
		if set -q result
			if test -n "$result"
				echo $result
			else
				eval $filename
				and echo
				fish_error "$key not found."
				set ret (math "$ret" + 1)
			end
		end
	end
	return $ret
end

function pdf2ref --description "try to find matching doi or isbn from pdf metadata"
	set -l pdffiles $argv
	for f in $pdffiles
		set -q pdffiles[2]
		and echo -n "$f: "

		set -l title (pdfgetinfo title $f)
		set -l authors (pdfgetinfo author $f)
		set -l year (pdfgetinfo date $f | sed -e 's/^.* \([0-9]\{4\}\) .*$/\1/g')

		fish_warning "not developed tool."
		echo $title
		echo $authors
		echo $year
	end
end
