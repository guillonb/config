#!/usr/bin/fish
#TODO: screenshot history file ; show last ; show history...

function screenshot --description "easy screenshotting using imagemagick's import"

	argparse -n "screenshot" --min-args 0 --max-args 0\
		-x 'h,m' -x 'h,s' -x 'h,p' -x 'h,d' -x 'h,o' -x 'h,n' -x 'o,n'\
		's/show' 'h/help' 'p/parents' 'q-silent'\
		'm/mode=!contains -- "$_flag_value" "full" "fullscreen" "f" "select" "selection" "sel" "s"'\
		'd/outdir=!test -d "$_flag_value"'\
		'o/out='\
		'n-outname=!builtin string match -v -r \'.*/.*\' $_flag_value'\
		-- $argv
	
	if set -q _flag_h
		echo "Usage: screenshot [-pqs] [-d=<out directory>] [--outname=<out file name>|--out=<out file path>]"
		return 0
	end

	if set -q _flag_o
		if builtin string match -r '.*/.*' $_flag_o
			if set -q _flag_d
				builtin string match -r '^/.*' $_flag_o
				and fish_warning "Output file '$_falg_o' specified with absolute path: output directory '$_flag_d' ignored."
				or set _flag_o "$_flag_d/$_flag_o"
			else
				set _flag_d (dirname $_flag_o)
			end
		else
			set -q _flag_d
			or set _flag_d "$HOME/pictures/screenshots"
			set _flag_o "$flag_d/$flag_o"
		end
		set _flag_n (basename $_flag_o)
	else
		set -q _flag_d
		or set _flag_d "$HOME/pictures/screenshots"
		set -q _flag_n
		or set _flag_n (date +%Y%m%d%H%M%S.png)
		set _flag_o "$_flag_d/$_flag_n"
	end
	
	set -q _flag_m
	or set _flag_m "fullscreen"

	set -q _flag_p
	and mkdir -p "$_flag_d"

	set -q _flag_q
	and set sound false
	or set sound true

	switch "$_flag_m"
		case "full" "fullscreen" "f"
			import -window root "$_flag_o"
			notify-send -a "screenshot" "Fullscreen captured" "saved to $_flag_o" &
		case "select" "selection" "sel" "s"
			import "$_flag_o"
			notify-send -a "screenshot" "Selection captured" "saved to $_flag_o" & 
	end
	and eval $sound
	and paplay /usr/share/sounds/freedesktop/stereo/camera-shutter.oga &

	if set -q _flag_s
		xdg-open "$_flag_o" &
		fish_verbose "displaying screenshot with PID=$last_pid"
	end

end

set -q $_
or screenshot $argv
