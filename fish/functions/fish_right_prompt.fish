function fish_right_prompt --description 'right prompt message'
	set -l availmodes
	set -l i 0
	for mode in $__active_prompt_modes
		set i (math $i + 1) 
		switch $mode
			case "+*" "@*"
				and set -a availmodes (string sub -s 3 H$mode)
		end
	end
	set -e i

	set -l mode_idx (math '(' $_prompt_mode - 1 ')' % (count $availmodes) + 1)
	set -l mode $availmodes[$mode_idx]
	#fish_debug "availmodes=$availmodes and _prompt_mode=$_prompt_mode thus mode_idx=$mode_idx whence mode=$mode" >&2
	switch $mode
		case "time" "default" ""
			fish_time_right_prompt
		case "return"
			fish_return_right_prompt
		case "locks"
			fish_locks_right_prompt
		case "git"
			fish_git_right_prompt
		case "hg"
			fish_hg_right_prompt
		case "svn"
		 	fish_svn_right_prompt
		case "*"
			if functions -q "fish_"$mode"_right_prompt"
				eval "fish_"$mode"_right_prompt"
			else
				echo $_prompt_unknownmodecolor'×'$_prompt_resetcolor
			end
	end
end

function fish_git_right_prompt --wraps fish_git_prompt --description 'right prompt function for git'
	echo $_prompt_repocolor"git"$_prompt_resetcolor(fish_git_prompt | string trim)
end

function fish_hg_right_prompt --wraps fish_hg_prompt --description 'right prompt function for hg'
	echo $_prompt_repocolor"hg"$_prompt_resetcolor(fish_hg_prompt | string trim)
end

function fish_svn_right_prompt --wraps fish_svn_prompt --description 'right prompt function for svn'
	echo $_prompt_repocolor"svn"$_prompt_resetcolor(fish_svn_prompt | string trim)
end

function fish_time_right_prompt --wraps date --description 'time right prompt'
	date +"$_prompt_timecolor%D—%H:%M$_prompt_resetcolor"
end

function fish_locks_right_prompt --description 'dynamic informative prompt'
	set -l locks
	command sudo -S fish -c ':' </dev/null 2>/dev/null
	and set -a locks $_prompt_lockcolor[1]"S"$_prompt_resetcolor
	or set -a locks $_prompt_lockcolor[2]"\$"$_prompt_resetcolor
	command pass unlock </dev/null 2>/dev/null
	and set -a locks $_prompt_lockcolor[1]"🔓"$_prompt_resetcolor
	or set -a locks $_prompt_lockcolor[2]"🔒"$_prompt_resetcolor
	for mycipher in (cipher list)
		cipher query $mycipher
		and set -a locks $_prompt_lockcolor[1](builtin string sub -l 1 $mycipher | tr '[a-z]' '[A-Z]')$_prompt_resetcolor
		or set -a locks $_prompt_lockcolor[2](builtin string sub -l 1 $mycipher | tr '[A-Z]' '[a-z]')$_prompt_resetcolor
	end
	builtin string join -- '' $locks
end

function fish_return_right_prompt --description 'show last command pipestatus and duration'
	set -l pst $pipestatus
	set -q _preserved_pipestatus
	and set -l pst $_preserved_pipestatus
	set -l rprompt (fish_format_pipestatus_for_prompt -ar -l'〉' $pst)
	set -a rprompt -- $_prompt_returncolor$CMD_DURATION"ms"$_prompt_resetcolor
	string join '·' $rprompt
end

function fish_format_pipestatus_for_prompt
	set -l _pipestatus $pipestatus
	argparse 'a/always' 'l/left=?' 'r/right=?' -- $argv

	#overwrite _pipestatus with $argv or $_preserved_pipestatus
	set -q _preserved_pipestatus[1]
	and set _pipestatus $_preserved_pipestatus
	set -q argv[1]
	and set _pipestatus $argv
	
	#whether an error occurred (2) or not (1) ⇒ _prompt_pipestatuscolor index
	set -l sfcol (echo $_pipestatus | grep -qw '[^0]'; and echo 2; or echo 1)

	if set -q argv[2] && test $sfcol -eq 2 || set -q _flag_a
		set -l lprompt

		if set -q _flag_l
			set -a lprompt $_prompt_pipestatuscolor[$sfcol]
			set -a lprompt (test -n "$_flag_l"; and echo $_flag_l; or echo '〈')
			set -a lprompt $_prompt_resetcolor
		end

		set -l pipestatusprompt
		for pst in $_pipestatus
			test $pst -eq 0
			and set -a pipestatusprompt $_prompt_pipestatuscolor[1]$pst$_prompt_resetcolor
			or set -a pipestatusprompt $_prompt_pipestatuscolor[2]$pst$_prompt_resetcolor
		end
		set -l pssep $_prompt_pipestatuscolor[$sfcol]'|'$_prompt_resetcolor
		set -a lprompt (string join -- $pssep $pipestatusprompt)

		if set -q _flag_r
			set -a lprompt $_prompt_pipestatuscolor[$sfcol]
			set -a lprompt (test -n "$_flag_r"; and echo $_flag_r; or echo '〉')
			set -a lprompt $_prompt_resetcolor
		end

		string join '' -- $lprompt
	end
end

function fish_right_prompt_loading_indicator -a last_prompt
	if test -n "$last_prompt"
		echo -n "$last_prompt" | sed -r 's/\x1B\[[0-9;]*[JKmsu]//g' | read -zl uncolored_last_prompt
		echo -n $_prompt_oldcolor"$uncolored_last_prompt"$_prompt_resetcolor
	else
		echo -n $_prompt_oldcolor"…"$_prompt_resetcolor
	end
end
