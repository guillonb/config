alias lsd "ls -cho --time-style=+%Y%M%d"
#alias cp "/bin/cp"
#alias mv "/bin/mv"
alias shuffle "sort -R"
#alias papis "env PYTHONWARNINGS=ignore papis"

set -q tmpfileext
and alias tmpfilepattern "echo '*.'(builtin string join '|*.' $tmpfileext)"
and alias treeX "/usr/bin/tree -I (tmpfilepattern)"

#alias ssh 'ssh 2>| sed "s/^\([a-z]*\)\(@\)\([a-zA-Z]*\|[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\.[0-9]\{1,3\}\)/(set_color brown)\1(set_color normal)\2(set_color -o cyan)\3/"'
begin;
	which klist; and which kinit;
end >/dev/null
and alias kssh "klist -s; or kinit brguillo@LOCAL.ISIMA.FR -k -t ~/.config/krb5/mykeytab.keytab; ssh -K"

alias caseine "/usr/local/bin/push"

alias whatismyip "curl -s http://ifconfig.me/"
alias pip3 "python3 -m pip"
