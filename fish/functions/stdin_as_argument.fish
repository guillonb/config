#!/usr/bin/fish
function from_stdin --description "read from stdin and feed fifo file(s) to be passed as argument of commmand"
	argparse -n from_stdin -is 'h/help' -- $argv

	if set -q _flag_h
		echo "from_stdin - preform process substitution that read from stdin"
		echo
		echo "   COMMAND1 | from_stin [-k | -K] [-p PLACEHOLDER] [-F | -f] [-s SUFFIX] COMMAND2" 
		echo
		echo "DESCRIPTION"
		echo "Executes COMMAND2 replacing each occurrence of PLACEHOLDER in COMMAND2 arguments"
		echo "by a fifo file fed by the output of COMMAND1. The output of COMMAND1 is teeed into"
		echo "as many fifo files as the number of PLACEHOLDER in COMMAND2 arguments."
		echo
		echo "EXAMPLE"
		echo '$> ls | from_stdin diff \'<>\' ls-save.txt'
		echo "   #(the same behavior can be obtained using `diff (echo \"bla\" | psub) ls-save.txt`)"
		echo '$> echo "foo" | from_stdin -p @@ diff @@ @@'
		echo '$> echo "foo" | from_stdin -k cat - @@ @@'
		echo
		echo "OPTIONS"
		echo "-f,  --file    pipe to regular file instead of named pipe (default, c.f. psub documentation)"
		echo "-F,  --fifo    pipe to named pipe instead of regular file (c.f. psub documentation)"
		echo "-k,  --keep-stdin"
		echo "    pipes COMMAND1 to COMMAND2 (this is the default)"
		echo "-K,  --do-not-keep-stdin"
		echo "    redirect the standard output of COMMAND1 to the created fifo (or regular) files only, so that"
		echo "    COMMAND2 standard input does not receive anything from COMMAND1, but has access to the parent"
		echo "    standard input"
		echo "-p PLACEHOLDER,  --placeholder PLACEHOLDER"
		echo "    sets the PLACEHOLDER string that should be replaced by the fifo file path (default is '<>')."
		echo "-s SUFFIX,  --suffix SUFFIX"
		echo "    append SUFFIX to the name of pipe or regular file (c.f. psub documentation)"
		echo
		return 0
	end


	argparse -n from_stdin -s -x 'f,F' -x 'k,K' 'k/keep-stdin' 'K/do-not-keep-stdin' 'p/placeholder=' 'F/fifo' 'f/file' 's/suffix=' -- $argv
	set -q _flag_p
	or set -l _flag_p "<>"
	set -q _flag_s
	and set -p _flag_s "--suffix"

	set -l fifos

	for i in (seq (count $argv))
		test "$argv[$i]" = "$_flag_p"
		or continue
		if set -q _flag_F
			set -a fifos "..."
			while 1
				set fifos[-1] (mktemp -u $_flag_s)
				mkfifo $fifos[-1]
				and break
			end
		else
			set -a fifos (mktemp $_flag_s)
		end
		set argv[$i] $fifos[-1]
	end

	set -l ret
	if set -q _flag_k; or not set -q _flag_K
		tee $fifos | eval $argv
		set ret $status
	else
		tee $fifos >/dev/null &
		set -l teepid $last_pid
		#TODO: read stdin from parent process stdin (is it possible?)
		eval $argv
		set ret $status
		kill $teepid 2>/dev/null
		wait
	end
	set -q fifos[1]
	and rm $fifos
end

