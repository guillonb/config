function randstr --description "generates random strings"
	argparse -i 'h/help' -- $argv

	set -q _flag_h
	and echo \
'Usage: randstr [ -c CHAR_SET ] [ LENGTH [ NUM ] ]

Arguments:
  LENGTH    set the length of the random strings to produce (postive integer expected, default is 1) 
  NUM       set the number of random strings to output (integer expected, default is 1);  If negative, infinitely many random strings are outputed.

Options:
  -c  --char CHAR_SET
            build string using chars from CHAR_SET (default is \'a-zA-Z0-9\'); CHAR_SET should be a valid CHAR_SET accepted by the tr command'
	and return 0

	argparse -i -X 2 'c/char=' -- $argv
	or return 1

	set -q _flag_c
	or set -l _flag_c 'a-zA-Z0-9'

	set -l length
	if set -q argv[1]
		not set length (math $argv[1])
		or test $length -le 0
		and fish_error "Positive integer expected!"
		and return 1
		or set -e argv[1]
	else
		set length 1
	end

	set -l num
	if set -q argv[1]
		not set num (math $argv[1])
		and fish_error "Integer expected!"
		and return 1
		or set -e argv[1]
	else
		set num 1
	end

	cat /dev/urandom | tr -dc $_flag_c | fold -w $length | head -n $num
end
