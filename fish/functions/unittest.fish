#TODO: separate unittest into two functions unittest (run) and __unittest (read)

function unittest --description "evaluate each line of stdin and report errors"
	argparse -n "unittest" -x 'q,v'\
		'h/help' 'q/quiet' 'r/return=?!math $_flag_value >/dev/null' 'v/verbose=?!math $_flag_value >/dev/null' -- $argv

	set -q _flag_h
	and echo "Usage: $_ [options] \"-\""
	and echo "       $_ [options] [<funcname>…]"
	and echo
	and echo "Description"
	and echo "  The function has two modes. Either it is called with  "
	and echo
	and echo "Options are:"
	and echo "  -h | --help             Display this help and return;"
	and echo "  -q | --quiet            Avoid displaying messages;"
	and echo "  -r | --return [<fd>]    Echo a return to file descriptor <fd> (default is 1);"
	and echo "  -v | --verbose [<lvl>]  Define or increase verbosity;"
	and echo
	and echo "Arguments are:"
	and echo "  <fd>        a file descriptor code (e.g., 1, 2, 3…);"
	and echo "  <lvl>       a integer;"
	and echo "  <funcname>  either a unittest function name __unittest_<name>, or a name. In"
	and echo "              the second case, all unittest functions matching <name> are taken."
	and echo
	and return 0

	set -l _old_Msg_lvl $Msg_lvl
	set -q _flag_v
	and test -n "$_flag_v"
	and set Msg_lvl $_flag_v
	or set -q _flag_v
	and set Msg_lvl (math $Msg_lvl + 1)
	or set -q _flag_q
	and set Msg_lvl 0

	switch "$argv[1]"
		case "help"
			exec $_ --help
		case "-"
			set -q argv[2]
			#and set -e argv[1]
			and fish_error -- "With '-' no other argument is expected; got $argv[2..-1]"
			and return 1

			set -l counter 0
			set -l succeed 0
			while read -l line
				set counter (math $counter + 1)
				eval $line
				and fish_valid -p "Passed" -- "[$counter]"
				and set succeed (math $succeed + 1)
				or fish_error -p "Failed" -- "[$counter] -- $line"
			end
			if set -q _flag_r
				test -n "$_flag_r"
				and echo "$succeed/$counter" >&$_flag_r
				or echo "$succeed/$counter"
			end
			set -gx Msg_lvl $_old_Msg_lvl
			return (math $counter - $succeed)
	end

	set -l funccounter 0
	set -l funcsucceed 0
	set -l cumcounter 0
	set -l cumsucceed 0

	set -q argv[1]
	and set -l seeds $argv
	or set -l seeds ""

	fish_debug -- "seeds: $seeds"

	for totest in $seeds
		set -l funcnames (functions --all | grep -e '^__unittest_' | grep $totest)
		for funcname in $funcnames
			set -l name (builtin string sub -s 12 "$funcname")
			fish_verbose -- "===== Testing $name ====="
			set funccounter (math $funccounter + 1)
			set globcounter (math $globcounter + 1)
			eval "$funcname" -r3 3>| read ret
			set -l counter (echo $ret | cut -d/ -f 2)
			set -l succeed (echo $ret | cut -d/ -f 1)
			set cumcounter (math $cumcounter + $counter)
			set cumsucceed (math $cumsucceed + $succeed)
			test $counter -eq $succeed
			and set funcsucceed (math $funcsucceed + 1)
			fish_verbose -- "Results: $succeed/$counter."
		end
	end
	if set -q _flag_r
		set ret "$cumsucceed/$cumcounter/$funcsucceed/$funccounter" 
		test -n "$_flag_r"
		and echo $ret >&$_flag_r
		or echo $ret
	end
	test $funccounter -gt 0
	and fish_verbose -- "===== All tests performed ====="
	fish_verbose -- "Results: $funcsucceed/$funccounter ($cumsucceed/$cumcounter)."
	set -gx Msg_lvl $_old_Msg_lvl
	return (math $cumcounter - $cumsucceed)
end

function __unittest_unittest
	begin
		echo "true"
		echo "echo 'true' | unittest -q '-'"
		echo "not begin; echo 'false' | unittest -q '-'; end"
		echo 'test "1/1" = (echo 'true' | unittest -r3 -q "-" 3>&1)'
		echo 'test "1/2" = (begin; echo "true"; echo "false"; end | unittest -r4 -q "-" 4>&1)'
		echo 'not unittest - something 2>/dev/null'
	end | unittest $argv "-"
end
