#!/usr/bin/fish

function color_lines
	argparse -i 'h/help' -- $argv
	
	set -q _flag_h
	and echo \
'Usage: alternate_color [-g COLOR_SPEC] [-H COLOR_SPEC] [-n N] [ COLOR_SPEC … ]

Alternately applies some style to lines read from standard input.

-g --global <COLOR_SPEC>
	add color specification COLOR_SPEC to each lines except heading
-<N> -n <N> --first-lines <N>
	display first N lines with different style (specified by -H option or "normal")
-H --head-style <COLOR_SPEC>
	apply style COLOR_SPEC to the N first lines (specified by -n option or 1)

COLOR_SPEC:
	any argument accepted by the set_color function, including options such as \'-o\'.
	See set_color --help for details.

If no positional argument is given, the color specification \'-r\' (reversed) is used.
If one positional argument only is found, it is applied to one line over two only.
If two or more color specifications are given, the line style alternate between them.'
	and return 0

	argparse -i 'g/global=+' 'n#first-lines' 'H/head-style=' -- $argv

	not set -q _flag_n
	and set -q _flag_H
	and set -l _flag_n 1

	not set -q _flag_n
	and set -l _flag_n 0

	not set -q _flag_H
	and set -q _flag_n
	and set -l _flag_H "normal"

	set -l colorspecs
	set -q argv[1]
	and set -- colorspecs[1] $argv[1]
	or set -- colorspecs[1] '-r'
	set -q argv[2]
	and set -- colorspecs[2] $argv[2]
	or set -- colorspecs[2] normal
	#colorspecs has length at least 2

	set -l i 1
	set -l c 0
	while read line
		if test $c -lt $_flag_n
			echo (set_color $_flag_H)"$line"(set_color normal)
			set c (math $c + 1)
		else
			echo (set_color $_flag_g (builtin string split ' ' -- $colorspecs[$i]))"$line"(set_color normal)
			set i (math $i + 1)
			set -q colorspecs[$i]
			or set i 1
		end
	end
end
