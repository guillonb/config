function string++ --wraps='builtin string' --description='allows to test whether the string $argv[2] is of some type class'
	set -l eoo (contains -- '--' $argv)
	and set -l posarg (builtin string match -rv -- '^-' $argv[1..$eoo]) $argv[(math $eoo + 1)..-1]
	or set -l posarg (builtin string match -rv -- '^-' $argv)

	switch $posarg[1]
		case "is"
			#src: https://stackoverflow.com/questions/52798129/in-fish-shell-how-to-check-if-a-variable-is-a-number
			set -l pattern
			set -l options '--quiet' '--regex'
			argparse 'Q/unquiet' -- $argv
			set -q _flag_Q
			and set -e options[1]
			switch $argv[2]
				case bool Boolean
					set -a options '-i'
					set pattern '^(?:[01tfyn]|true|false|yes|no)$'
				case I int integer
					set pattern '^[+-]?\d+$'
				case +I +int +integer positive
					set pattern '^[+]?\d+$'
				case -I -int -integer negative
					set pattern '^-\d+$'
				case H hex hexadecimal xdigit
					set pattern '^[[:xdigit:]]+$'
				case O oct octal
					set pattern '^[0-7]+$'
				case B bin binary
					set pattern '^[01]+$'
				case F float double
					set pattern '^[+-]?(?:\d+(\.\d+)?|\.\d+)$'
				case +F +float +double
					set pattern '^[+]?(?:\d+\(.\d+)?|\.\d+)$'
				case -F -float -double
					set pattern '^-(?:\d+\(.\d+)?|\.\d+)$'
				case A alpha alphabetic
					set pattern '^[[:alpha:]]+$'
				case AN alnum alphanum alphanumeric
					set pattern '^[[:alnum:]]+$'
				case T time
					set pattern '^(?:\d{,4,6,8,10,12,14}|\d{14}-\d{,2,4,6,8,10,14}|\d{12}-\d{,2,4,6,8,12}|\d{10}-\d{,2,4,6,10}|\d{8}-\d{,2,4,8}|\d{6}-\d{,2,6}|\d{4}-\d{4})$'
				case '*'
					echo "unknown class..." >&2
					return 1
			end
			set -- argv match $options -- $pattern $argv[3..-1]
	end
	if isatty
		builtin string $argv
	else
	 	cat | builtin string $argv
 	end
end
