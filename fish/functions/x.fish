command -s wmctrl >/dev/null 2>&1
and alias max 'wmctrl -r :ACTIVE: -b toggle,maximized_horz,maximized_vert'

command -s xclip >/dev/null 2>&1
and alias ctrlc 'xclip -i -selection clipboard'
and alias ctrlv 'echo -n -- (xclip -o -selection clipboard)'
