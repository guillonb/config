function mkdircd -d 'Make a directory and enter it'
	mkdir $argv
	and cd $argv[-1]
end

function rmdirtree -d 'remove all subdirectory if all are empty'

	argparse --name="rmdirtree" 'f/force' -- $argv

	set -q argv[1]
	or set argv "."

	for dir in $argv
		test ! -d "$dir"
		and fish_error "directory '$dir' not found or not a directory."
		and return 2

		if ! set -q _flag_f
			set -l found_file (find "$dir" -type f -print -quit)
			test -n "$found_file"
			and fish_error "Some files found. Remove aborted."
			and return 3
		end

		#remove all $dir's subdirs and $dir (if empty and not pwd)
		find $dir -type d -exec rmdir -p '{}' \; ^/dev/null

		set -l absolute_dir (readlink -f "$dir")
		if test -d "$absolute_dir"
			test "$absolute_dir" = (readlink -f (pwd))
			and cd ..
			and if ! rmdir "$absolute_dir"
				cd "$absolute_dir" #if failed, rollback `cd`
				return 3
			end
		end
	end
end

function rmdirpwd -d 'recursively remove current directory until reaching an non-empty directory'
	while rmdir (pwd) ^/dev/null
		cd ..
	end
end
