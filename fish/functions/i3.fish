function i3-execute -d "wrapper to run command in i3 framework"
	argparse -i --min-args 1 'd/delay=' 'h/help' 't/terminal' 'w/workspace=' 'f/focus=?' -- $argv

	set -q _flag_h
	and echo "Usage: $_ --help" 
	and echo "       $_ [--delay=<t>] [--focus[=<f>]] [--terminal] [--workspace=<ws>] [--] <command> [args…]"
	and echo
	and echo "<t> is the delay to sleep before refocusing the initially focused window;"
	and echo
	and echo "<f> is one of {1, true, yes, 0, false, no} or the empty string (interpreted as true);"
	and echo
	and echo "<ws> defines the workspace name in which to run <command>, as follows:"
	and echo " — if it is ., =, or empty, then it is replaced by the current workspace name;"
	and echo " — if it is ++, or + (resp. --, or -) then it is replaced by +1 (resp. -1);"
	and echo " — if the arithmetic operation (math <current worspace> <ws>) succeed, then its"
	and echo "      result is used as target workspace name;"
	and echo " — otherwise, if <ws> starts with !, then the first occurrence of ! in <ws> is"
	and echo "      dropped, and the resulting string is chosen as target workspace name;"
	and echo " — otherwise, <ws> is taken as target workspace name."
	and echo
	and echo "if --terminal is given then <command> is run a terminal (xterm)."
	and echo
	and return 0

	set -q _flag_d
	or set -l _flag_d .25

	set -q _flag_f
	and contains -- $_flag_f 0 false no
	and set -e _flag_f

	set -q _flag_f 
	or set -l unfocus (xdotool getwindowfocus)

	set -l -- command (builtin string escape -- $argv)
	set -q _flag_t
	and set -- command "xterm -e '$command'"
	set -l -- command (builtin string join -- ' ' "exec" "cd" (builtin string escape -- (pwd)) "&&" $command) 

	if set -q _flag_w
		set -l current_workspace (i3-get_focused_workspace)
		switch $_flag_w
			case "." "=" ""
				set -- _flag_w $current_workspace
			case "++" "+" "--" "-"
				set -- _flag_w (builtin string sub -s 1 -l 1 $_flag_w)"1"
		end
		set -- new_workspace (math $current_workspace $_flag_w 2>/dev/null)
		or begin
		switch $_flag_w
			case "!*"
				set -- new_workspace (builtin string sub -s 2 -- $_flag_w)
			case "*"
				set -- new_workspace $_flag_w
			end
		end
		fish_debug "[$_] Selected workspace is '$new_workspace'"


		if test "$new_workspace" != "$current_workspace"
			set -l -- pre "workspace \"$new_workspace\""
			#set -- post "workspace \"$current_workspace\""
			set -l -- post "workspace back_and_forth"
			set -- command (builtin string join -- '; ' $pre $command $post)
		
			set -q unfocus
			and set -e unfocus
		end
	end

	fish_debug "[$_] Command sent to `i3-msg` is '$command'"
	i3-msg -qt command -- $command

	set -q unfocus
	and fish_debug "wmctrl refocus"
	and sleep $_flag_d
	and wmctrl -ia $unfocus
	or true
end
function i3-open --wraps i3-execute --description 'combine i3-execute and xdg-open to open document with i3'
	argparse -i 'c/command=!type -q \'$_flag_value\'' -- $argv
	fish_debug "[$_] command to use: '"(set -q _flag_c; and echo $_flag_c; or echo xdg-open)"'"
	set -q _flag_c
	and set -l command "$_flag_c"
	or set command "xdg-open"
	set -l options $argv
	set -p options "-f" "-w="
	argparse -i --min-args 1 'd/delay=' 'h/help' 't/terminal' 'w/workspace=' 'f/focus=?' -- $argv
	set -l options (for o in $options; contains -- $o $argv; or echo $o; end)
	for file in $argv
		if builtin string match -qre '^https?://.*$' -- (builtin string trim -- $file)
			set -l command "x-www-browser"
		else if not set -q _flag_c
			set -l dsk
			if test -e $file
				set dsk (xdg-mime query default (xdg-mime query filetype $file))
				or return 1
			else
				not builtin string split -q '.' -- $file
				and fish_error "$file: no such file or directory"
				and return 1
				set -l ext (builtin string split '.' -- $file)[-1]
				not grep -qe '\s'$ext'\(\s.*\)\?$' /etc/mime.types
				and fish_error "$ext: unknown file extension"
				and return 1
				set -l mimet (grep -e '\s'$ext'\(\s.*\)\?$' /etc/mime.types | cut -f1)
				set dsk (xdg-mime query default $mimet)
				or return 1
			end
			test -f "/usr/share/applications/$dsk"
			and set dsk "/usr/share/applications/$dsk"
			or set dsk (locate "$dsk")
			fish_debug "desktop found: $dsk"
			grep -qie '^\s*Terminal\s*=\s*True\s*$' "$dsk"
			and set -a -- options "-t"
		end
		i3-execute $options $command "$file"
	end
end
alias i3-get_focused_workspace 'i3-msg -t get_workspaces | jq \'map(select(.focused))|.[0].num\''
alias i3-less 'i3-execute -t -f -w= less'
alias i3-cless 'i3-execute -t -f -w= cless'
