function onlyfiles --description 'find all files in directory'
	if test -z "$argv"
		find . -type f -print
	else
		for i in $argv
			find $i -type f -print
		end
	end
end
