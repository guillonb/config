function write2 --description "write stdin to file with overwriting confirmation prompt"
	argparse -N 1 -X 1 'h/help' -- $argv
	if set -q _flag_help
		echo "Usage: $funcname <output file>"
		echo "       $funcname --help"
		return 0
	end

	test -e "$argv[1]"
	or cat >"$argv[1]"

	echo "writing to $argv[1]"

	not test -f "$argv[1]"
	and fish_error "file $argv[1] exists but is not regular. Abort"
	and return 2

	read -l confirm -P "Overwrite $argv[1] (y/N)? " <&3
	if not contains -- "$confirm" y Y yes Yes YES 1
		fish_error "Abort"
		test -z "$confirm"
		or contains -- "$confirm" n N no No NO 0
		return $status
	end

	cat >"$argv[1]"
end
