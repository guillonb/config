function inlineoutput -d 'outputs the content of multiple lines in one line, separated by delimiter'

	argparse --name=inlineoutput --min-args 0 --max-args 0 'l/leading=' 'd/delimiter=' 't/trailing=?' -- $argv

	set -q _flag_d
	and set -l delimiter "$_flag_d"
	or set -l delimiter " "

	set -q _flag_l
	and set -l leading "$_flag_l"

	set -q _flag_trailing
	and test "$_flag_t" = ""	
	and set -l trailing "$delimiter"
	or set -l trailing "$_flag_t"

	cat - | \
		while read line
			eval $firstline
			and echo -n "$leading"
			and set firstline false
			or echo -n "$delimiter"
			echo -n "$line"
		end
	echo -n "$trailing"
end
