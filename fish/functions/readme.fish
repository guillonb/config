function readme -d "open a readme with correct viewer"
	set rmf (find $argv[1] \( -iname 'readme.*' -o -iname 'readme' \) -type f -print)

	test -z "$rmf"
	and fish_error "No readme file found"
	and return 1

	set -q rmf[2]
	and fish_warning "Several readme files found ($rmf). Select first one"

	if echo $rmf[1] | grep -qie '^readme\(.txt\)\?$'
		less $rmf[1] 
	else
		open $rmf[1]
	end
end
