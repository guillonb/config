function name++ -d 'increment integer in file/dir names'
	#todo:
	# + options:
	#   + -d	| --decrement		with optional argument n (decremental step) : "-d n" equivalent to "-d -n n"
	#   + -b	| --by				with argument n (incremental step)
	#   + -I	| --index			with argument n index of the integer to be incremented
	#   + 	  --date				look for date format
	#   + -r | --recursive		recursive
	#   + -p | --print			only print target, do not move file
	#   mv options:
	#   + -i	| --interactive	interactive mv
	#   + -v | --verbose			verbose mv
	#   + -f | --force			force mv
	# + multiple files
	echo $argv[-1] | egrep '[0-9]' >/dev/null
	and set -l lastnum (math (echo $argv[-1] | sed -e 's;^.*\([0-9]\+\)[^0-9]*$;\1;g') + 1)
	and mv -i $argv[-1] (echo $argv[-1] | sed -e "s;^\(.*[^0-9]\)\?[0-9]\+\([^0-9]*\)\$;\1$lastnum\2;g")
end

function cofi --description "toggle extension '.comment' of files..."
	for i in $argv
		echo $i | egrep '^.$\.comment$' >/dev/null
		and rename 's/\.comment$//g' $i
		or rename 's/$/.comment/g' $i
	end
end
