#depends on message.fish
function recreadlink --description='Read symbolic link'

	set -l exitcode 0
	set -l followarg
	set -l prevpath

	for arg in $argv
		while test -n "$arg"
			if test -e "$prevpath$arg"

				set followarg (readlink (builtin string replace -r '/$' '' "$prevpath$arg"))
				builtin string match -r '^/.*' -q "$followarg"
				and set prevpath ""
				or set prevpath (realpath -s (dirname "$prevpath$arg"))"/"

				if test -z "$followarg"
					test -d "$prevpath$arg"
					and set_color -o blue
					or set_color normal
					echo "$arg"
				else
					set_color -o cyan
					echo -n "$arg"
					set_color normal
					echo -n " → "
				end

			else

				fish_error "'$prevpath$arg' file not found."
				set exitcode (math $exitcode + 1)
				set followarg ""
			end

			set arg "$followarg"
		end
		set prevpath ""
	end

	return $exitcode
end
