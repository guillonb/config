function decpath -d 'decompose a path by listing each ancestor directory'

	argparse -i -n 'decpath' 'h/help' 't/truncate' 'c/cut=' 'T-trailing-slash' 's/separator=?' -- $argv

	set -q _flag_h
	and echo "Usage: $_ [options]  [<path>…]"
	and echo
	and echo "Result is:"
	and echo "  enumeration of each ancestor of each <path> given as argument."
	and echo
	and echo "Options are:"
	and echo "  -c | --cut   <path>      stop backward enumeration when encountring <path>;"
	and echo "  -h | --help              show this help message and return;"
	and echo "  -s | --separator [<sep>] use <sep> as path <separator> (default is ' ');"
	and echo "       --trailing-slash    append a trailing slash to the end of output paths;"
	and echo "  -t | --truncate          transform cut <path> to its deepest common ancestor with considered <path>."
	and echo
	and return 0

	set -q argv[1]
	and set -l paths $argv
	or set -l paths "."

	set -q _flag_s
	and test -n "$_flag_s"
	and set -l sep "$_flag_s"
	or set -l sep " "

	for path in $paths
		set -q _flag_c
		and set -l cutpath "$_flag_c"
		or set -l cutpath "/"

		#Canonicalize paths
		test "$path" = "."
		and set path (pwd)
		test "$path" = "/"
		or set path (builtin string trim -c/ -r "$path")

		test "$cutpath" = "."
		and set cutpath (pwd)
		test "$cutpath" = "/"
		or set cutpath (builtin string trim -c/ -r "$cutpath")

		#Reversed cut ⇒ return 0
		test "$path" != "$cutpath"
		and builtin string match -qr '^'"$path" "$cutpath"
		and return 0

		#Check cutpath or get common ancestor
		while not builtin string match -qr '^'"$cutpath" "$path"
			set -q _flag_t
			and set cutpath (dirname "$cutpath")
			and fish_comment "taking '$cutpath' as cutpath instead"
			and continue
			fish_error -s "" "The cut path should be a subpath of the given paths"
			fish_error -p "   " " ↘ got '$cutpath' and '$path', respectively"
			return -1
		end

		set -l stopflag true
		while eval $stopflag
			#Check stop condition
			test "$path" != "$cutpath"
			or set stopflag false

			#Format output
			set -q _flag_T
			and test "$path" != "/"
			and set -l output "$path/"
			or set -l output "$path"

			#Display output (pipe aware)
			not command test -t 1
			or set -q _flag_s
			and echo -n "$output$sep"
			or echo "$output"
			
			#Set path to parent
			set path (dirname "$path")
		end
	end
end

#UNIT TEST
function __unittest_decpath
	begin
		echo 'diff (decpath | psub) (decpath (pwd) | psub)'
		echo 'diff (decpath | psub) (decpath "." | psub)'
		echo 'diff (decpath ~ | psub) (decpath ~$USER | psub)'
	end | unittest $argv '-'
end

