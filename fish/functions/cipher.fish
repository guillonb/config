## todo: catch Ctrl-C (SIGINT) and call "cipher clean"
function cipher --description 'mount and unmount gocrypt cipher directory'

	set -q cipher_conf
	or set -l cipher_conf (xdg-user-dir CONFIG)/cipherrc

	argparse --name "cipher" \
		#-x 'h,l' -x 'h,i' -x 'h,c' \
   		'h/help' 'i-ignore-warnings' 'l/update-locks' 'c/configuration-file=' \
		-- $argv

	#Displaying help
	test "$argv[1]" = "help" -o "$argv[1]" = "h"
	and set -l _flag_h $argv[1]
	if set -q _flag_h
		echo "Usage: cipher [<options>] [<mode> [<cipher>]...]"
		echo
		echo "+ <mode>:"
		echo "   {cd,clean,close,go,help,list,lock,mount,open,query,status,toggle,unlock,unmount}  (default: 'status');"
		echo "+ <cipher>:"
		echo "   line key of **required** configuration file (default is all found keys)."
		echo
		echo "+ <options>:"
		echo "   -c <path>|--configuration-file=<path>"
		echo "                  specifies a path to an alternative configuration file."
		echo "   -h  --help     just as help mode: display this help and exit"
		echo "   --ignore-warnings"
		echo "                  ignore warning messages. "
		echo "   -l | --update-lock"
		echo "                  force lock update when (un)mounting (un)mounted cipher."
		echo
		echo "The default configuration file is `"$cipher_conf"`."
		return 0
	end

	#Finding configuration file
	set -l conf
	if set -q _flag_c
		set conf _flag_c
		test ! -f "$conf"
		and fish_error "$conf: no such file. Abort."
		and return 1
	else
		set conf "$cipher_conf" 
		test ! -f "$conf"
		and fish_error "No configuration file found. Abort."
		and return 1
	end

	#Determining mode
	set -q argv[1]
	and set -l mode "$argv[1]"
	and set -e argv[1]
	or set -l mode "status"

	#Listing ciphers to consider
	set -q argv[1]
	and set -l ciphers $argv
	or set -l ciphers (grep -ve '^$' -e '^#' -e '^\s*$' "$conf" | cut -d: -f1)

	#Operating ciphers
	for cipher in $ciphers

		set -l cipherConf		(grep -e '^'$cipher':' $conf | sed 's/\t\t*/\t/g')
		set -l cipherDir		(eval echo (echo $cipherConf | cut -f 2))
		set -l cipherMountPoint	(eval echo (echo $cipherConf | cut -f 3))
		#set -l cipherDescr		(eval echo (echo $cipherConf | cut -f 4))
		set -l cipherLock		(eval echo (echo $cipherConf | cut -f 5))
		switch "$cipherLock"
			case true 1
				set cipherLock true
			case false 0
				set cipherLock false
			case ""
				set cipherLock false
			case *
				fish_error "$cipher: unknown lock configuration '$cipherLock'"
				return 1
		end

		test -z "$cipherConf"
		or test -z "$cipherDir"
		or test -z "$cipherMountPoint"
		or test -z "$cipherLock"
		and fish_error "'$argv[2]': cipher not found or invalid cipher settings"
		and return 2
		
		df | grep -qe '^'"$cipherDir"
		and set -l cipherStatus true
		or set -l cipherStatus false

		switch $mode
			case "clean" "c"
				if $cipherStatus
					# recursive call to cipher (unlock)
					cipher unlock "$cipher"
				else
					# recursive call to cipher (lock)
					cipher lock "$cipher"
				end
			case "close"
				if $cipherStatus
					# recursive call
					cipher unmount "$cipher"
				end
				cd "$cipherMountPoint/.."
			case "list" "ls"
				echo $cipher
			case "lock" "l"
				eval $cipherLock
				or return
				if /usr/bin/chmod u-w "$cipherMountPoint"
					fish_valid "write permission on $cipherMountPoint have been removed for $USER"
				else
				   	fish_error "unable to remove $USER write permission for $cipherMountPoint. Abort"
					return 5
				end
			case "unlock" "L"
				eval $cipherLock
				or return
				if /usr/bin/chmod u+w "$cipherMountPoint"
					fish_valid "write permission on $cipherMountPoint have been granted to $USER"
				else
					fish_error "unable to add $USER write permission for $cipherMountPoint. Abort"
					return 5
				end
			case "go" "open"
				if ! $cipherStatus
					fish_verbose "$cipher is not mounted. Mounting it"
					#recursive call (mount $cipher)
					cipher mount "$cipher"
				end
				and cd "$cipherMountPoint"
			case "cd"
				set -q argv[2]
				and fish_error "in 'cd' mode, only one cipher argument is expected"
				and return 3
				eval $cipherStatus
				or fish_warning "entering unmounted cipher mount point ($cipher)."
				cd "$cipherMountPoint"
			case "mount" "m"
				if eval $cipherStatus
					set -q _flag_i
					or set -q _flag_ignore_warnings
					or fish_warning "$cipher is already mounted (at `$cipherMountPoint`)"
					#recursive call (grant write permission to $USER for $cipherMountPoint)
					set -q _flag_l
					and cipher unlock "$cipher"
				else
					#recursive call (grant write permission to $USER for $cipherMountPoint)
					cipher unlock "$cipher"
					or return $status
					gocryptfs "$cipherDir" "$cipherMountPoint"
					or begin
						#recursive call (decline write permission to $USER for $cipherMountPoint)
						cipher lock "$cipher"
						or return $status
					end
				end
			case "query" "q"
				eval $cipherStatus
				and set answer (math $answer + 1)	
			case "status" "s"
				eval $cipherStatus
				and echo "$cipher: mounted at `$cipherMountPoint`."
				or echo "$cipher: unmounted."
			case "toggle" "t"
				if $cipherStatus
					#recursive call (unmount $cipher)
					cipher unmount "$cipher"
				else
					#recursive call (mount $cipher)
					cipher mount "$cipher"
				end
			case "unmount" "u"
				if eval $cipherStatus
					fusermount -u "$cipherMountPoint"
					#recursive call (decline write permission to $USER for $cipherMountPoint)
					and cipher lock "$cipher"
				else
					set -q _flag_i
					or set -q _flag_ignore_warnings
					or fish_warning "$cipher is not mounted"
					#recursive call (decline write permission to $USER for $cipherMountPoint)
					set -q _flag_l
					and cipher lock "$cipher"
				end
		end
	end
end
