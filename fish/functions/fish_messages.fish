#some variable used are defined in variables.d/messages.fish. The function is robust.

function fish_messages --description 'Message tool function'
	argparse -i -n "$_" 'h/help' -- $argv

	#print help
	set -q _flag_h
	and echo "Usage: $_ [options] [--] [echo options] [<S>…]"
	and echo
	and echo "Where options are:"
	and echo " -c <C> | --color <C>         set message color"
	and echo " -C <C> | --more-color <C>    append other message color (repeatable)"
	and echo " -S     | --show-defaults     show the parameter values and return"
	and echo " -f <F> | --file <F>          set the file to which to write the message"
	and echo " -h     | --help              display this help and return"
	and echo " -l <N> | --level <N> | -<N>  set the message level"
	and echo " -L <N> | --assume-level <N>  set (override) the current level"
	and echo " -n                           do not output newline"
	and echo " -p <S> | --prefix <S>        set the message prefix"
	and echo " -s <S> | --suffix <S>        set the message suffix"
	and echo
	and echo "Arguments are:"
	and echo " <C> any style list accepted by the fish set_color builtin"
	and echo " <F> any file name, or a file descriptor prefixed by '&'"
	and echo " <N> any integer"
	and echo " <S> any string to be displayed"
	and echo
	and echo "Default parameter value for $_ are:"
	and $_ -D $argv
	and echo
	and return 0

	argparse -i -n 'fish_message'\
		'c/color=!eval set_color $_flag_value >/dev/null'\
	   	'C/more-color=+!eval set_color $_flag_value >/dev/null'\
	   	'S/show-defaults' 'f/file=' 'h/help'\
	   	'n' 'l#level'\
	   	'L/assume-level=!math $_flag_value >/dev/null'\
	   	'p/prefix=' 's/suffix=' -- $argv

	#default parameter values
	set -q _flag_l
	and set -l msg_lvl $_flag_l
	or set -l msg_lvl 1

	set -l cur_lvl
	if set -q _flag_L
		set cur_lvl $_flag_L
	else
		set -q Msg_lvl
		and test -n "$Msg_lvl"
		and set cur_lvl (math $Msg_lvl)
		or set cur_lvl 1
	end

	set -q _flag_f
	and set -l file $_flag_f
	or set -l file '&1'

	set -q _flag_c
	and set -l color $_flag_c
	or set -l color "normal"

	set -q _flag_C
	and set -a color $_flag_C

	set -q _flag_p
	and set -l prefix $_flag_p

	set -q _flag_s
	and set -l suffix $_flag_s

	set -q _flag_n
	and set -l eol
	or set -l eol \n

	#show parameter values
	if set -q _flag_S
		echo "color:   $color"
		echo "file:    $file"
		echo "msg_lvl: $msg_lvl"
		echo "cur_lvl: $cur_lvl"
		echo "prefix:  $prefix"
		echo "suffix:  $suffix"
		return 0
	end

	#debug:
	#echo "cur_lvl: $cur_lvl; msg_lvl: $msg_lvl; color: $color; prefix: $prefix; suffix: $suffix; file: $file; argv: $argv" >&2

	#print message
	not test $cur_lvl -ge $msg_lvl; and true	#exit status trick
	or printf (eval set_color $color)"$prefix$argv$suffix"(set_color normal)"$eol" | \
		begin
			if builtin string match -qr '^&[0-9]+$' "$file"
				set -l fdnum (builtin string trim -c\& -l "$file")
				cat - >&$fdnum
			else
				cat - >$file
			end
		end
end

#aliases for fish_messages.fish
alias fish_comment	'fish_messages -l $Com_level -p "$Com_prefix" -s "$Com_suffix" -c "$Com_color" -f $Com_fd'
alias fish_debug		'fish_messages -l $Dbg_level -p "$Dbg_prefix" -s "$Dbg_suffix" -c "$Dbg_color" -f $Dbg_fd'
alias fish_error		'fish_messages -l $Err_level -p "$Err_prefix" -s "$Err_suffix" -c "$Err_color" -f $Err_fd'
alias fish_valid		'fish_messages -l $Val_level -p "$Val_prefix" -s "$Val_suffix" -c "$Val_color" -f $Val_fd'
alias fish_verbose	'fish_messages -l $Vrb_level -p "$Vrb_prefix" -s "$Vrb_suffix" -c "$Vrb_color" -f $Vrb_fd'
alias fish_warning	'fish_messages -l $War_level -p "$War_prefix" -s "$War_suffix" -c "$War_color" -f $War_fd'
