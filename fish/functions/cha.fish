function rdv -d "open rendez-vous.renater.fr"
	argparse -i 'h/help' -- $argv
	set -q _flag_h
	and echo "Usage: $_ [i3-execute options] [--] [x-www-browser options] [--] [<url>…] <rdv>"
	and return 0

	argparse -i 'd/delay=' 'w/workspace=' 'f/focus=?' -- $argv

	not set -q argv[-1]
	and $_ --help
	and return 1

	set -l i3options
	set -q _flag_d; and set -a -- i3options -d=$_flag_d; and fish_debug "_flag_d: '$_flag_d'"
	set -q _flag_f; and set -a -- i3options -f$_flag_f; and fish_debug "_flag_f: '$_flag_f'"
	set -q _flag_w; and set -a -- i3options -w=$_flag_w; and fish_debug "_flag_w: '$_flag_w'"

	set -l -- urlprefix "http"
	set -l -- urlprefixlen (builtin string length $urlprefix)

	set -l -- url $argv[-1]
	and set -e argv[-1]

	test "$urlprefix" = (builtin string sub -l $urlprefixlen (builtin string lower "$url"))
	or set -- url "https://rendez-vous.renater.fr/$url"

	fish_verbose "Opening $url…"
	fish_debug "[$_] i3-execute called with i3options: $i3options"
	i3-execute (builtin string split -- ' ' $i3options) -- x-www-browser $argv "$url"
end

function chaforward -d "start/check/exit/restart ssh tunnel to cha"
	mkdir -p ~/.ssh/tunnels/
	switch $argv[1]
		case "check"
			ssh -o ControlPath=~/.ssh/tunnels/%r@%h:%p.conn -O check cha 2>/dev/null
		case "exit"
			ssh -o ControlPath=~/.ssh/tunnels/%r@%h:%p.conn -O exit cha 2>/dev/null
		case "start" "open" "create"
			ssh -MfNR 22222:localhost:22 -o ExitOnForwardFailure=yes -o 'ControlPath=~/.ssh/tunnels/%r@%h:%p.conn' cha
		case "restart"
			chaforward check
			and chaforward exit
			chaforward start
		case ""
			chaforward start
		case '*'
			fish_error "Invalid argument $argv[1]"
			return 1
	end
end

function ndtmux -d "open tmux on networkdisk with shared socket with cha"
	not cipher query work
	and echo "cipher work is not mounted"
	and cipher mount work

	cipher query work
	or exit -1

	chaforward check || chaforward create

	bash -c "sleep 1; chgrp trust /tmp/4cha" &
	if tmux -S /tmp/4cha list-session &>/dev/null
		tmux -S /tmp/4cha attach
	else
		tmux -S /tmp/4cha new -c /home/guillonb/documents/work/research/prgm/networkdisk -t networkdisk
	end
	wait
end

alias chawork "not cipher query work; and echo \"cipher work is not mounted\"; and cipher mount work; rdv -f'no' -- 'https://mattermost.cristal.univ-lille.fr/links/channels/networkdisk' 'https://jitsi.dsi.uca.fr/networkdisk'; ndtmux"
