function xml_pp --description "pretty print xml files using xmllint and pygmentize"
	set -q argv[1]
	or set argv[1] "-"
	xmllint --format $argv | pygmentize -l xml
end
