function help+ -d 'additional help when not found or when \'--personal\' option is given'

	set -l docpath "$HOME/prgm/doc"

	test "$argv[1]" = "--personal"
	and set -l tryhelp false
	and set -e argv[1]
	or set -l tryhelp true
	
	test "$argv[1]" = "--"
	and set -e argv[1]

	if test -z "$argv[1]"
		set_color red
		echo -n "Error: " >&2
		set_color normal
		echo "argument expected" >&2
		return 1
	else
		set -l command $argv[1]
		set -e argv[1]
	end

	test -n "$argv[1]"
	and set_color yellow
	and echo -n "Warning: " >&2
	and set_color normal
	and echo "'$argv' ignored. Only one argument expected" >&2

	eval $tryhelp; and help $command ^/dev/null
	if test $status -ne 0
		set -l docfiles \
			( find $docpath	-name "$command"			-type f -print )\
	  		( find "$docpath" -name "$command.md"		-type f -print )\
			( find "$docpath" -name "$command.pdf"		-type f -print )\
			( find "$docpath" -name "$command.html"	-type f -print )\
			( find "$docpath" -name "$command.url"		-type f -print )\
			( find "$docpath" -name "$command*" 		-type f -print )
		if test -n "$docfiles"
			set_color white
			echo -n "Info: "
			set_color normal
			echo -n (for i in $docfiles; echo -n (basename "$i") " -- "; end); echo
			echo "$docfiles" | awk 'NR==1 {print $1}'
			echo 'xdg-open (echo "$docfiles" | awk \'NR==1 {print $1}\')'
			xdg-open (echo "$docfiles" | awk 'NR==1 {print $1}')
		else
			set_color red
			echo -n "Error: " >&2
			set_color normal
			if eval $tryhelp
				echo
				help $command
			else
				echo "no personal documentation found." >&2
			end
		end
	end
end
