function fish_left_prompt --description 'Print the beautiful left prompt'
	set -l last_cmd_status $status $pipestatus
	set -q _preserved_pipestatus[1]
	and set -l last_cmd_status $_preserved_pipestatus[-1] $_preserved_pipestatus
	set -l lprompt
	set -l shortmode (test (tput cols) -lt 45; and echo true; or echo false)

	#USER
	set -a lprompt $_prompt_usercolor
	set -a lprompt (echo $USER | if $shortmode; sed -e 's/^\(.\).*\$/\1/'; else; sed -e 's/^guillonb$/g/g'; end)
	set -a lprompt $_prompt_resetcolor

	set -a lprompt "@"

	#HOST
	set -l hostcolidx (test -n "$SSH_CLIENT"; and echo 2; or echo 1)
	set -a lprompt $_prompt_hostcolor[$hostcolidx]
	set -a lprompt (echo $hostname | if $shortmode; sed -e 's/^\(.\).*\$/\1/'; else; sed -e 's/^Gacrux$/G/g'; end)
	set -a lprompt $_prompt_resetcolor

	set -a lprompt ":"

	#PATH
	set -a lprompt $_prompt_pathcolor
	set -a lprompt (pwd | short_path)
	set -a lprompt $_prompt_resetcolor

	#STATUS
	if test $last_cmd_status[1] -eq 0
		set -p lprompt $_prompt_statuscolor[1]'['$_prompt_resetcolor
		set -a lprompt $_prompt_statuscolor[1]']'$_prompt_resetcolor
	else
		set -p lprompt $_prompt_statuscolor[2]'['$_prompt_resetcolor
		set -a lprompt $_prompt_statuscolor[2]']'$_prompt_resetcolor
	end

	string join -- '' $lprompt '$ '
end

function fish_left_prompt_loading_indicator -a last_prompt
	echo -n "$last_prompt" | sed -r 's/\x1B\[[0-9;]*[JKmsu]//g' | read -zl uncolored_last_prompt
	echo -n $_prompt_oldcolor"$uncolored_last_prompt"$_prompt_resetcolor
end

alias fish_prompt fish_left_prompt
