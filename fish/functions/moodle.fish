function csvstudents_from_rendumoodle -d "write a csv student list file from a repository of moodle works"
	argparse 'h/help' 'o/output=' 'a/append' -- $argv
	set -q _flag_o
	and set outfile (basename $_flag_o .csv).csv
	or set outfile "/dev/stdout"

	set -q argv[1]
	and set -l repos (builtin string trim -c '/ ' "$argv")
	or set -l repos (ls -d ./rendu*)

	set -q _flag_a
	or echo "lastname;name;number" > "$outfile"

	for repo in $repos
		for sdir in $repo/*
			test -d $sdir
		   	or continue
			set -l sn (builtin string split '_' (basename $sdir))
			set -l naming (builtin string split ' ' $sn[1])
			set -l name $naming[1]
			set -l lastname $naming[2]
			set -l number $sn[2]
			echo "$lastname;$name;$number"
		end
	end >> $outfile
end

