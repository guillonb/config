function mustache --description "wrapper of chevron (python mustache) tool"
	argparse -N 1 -i -n mustache 'K/keep-empty-lines=?' 'd/data=' 'o/output=' -- $argv
	#argparse bug?
	set -q _flag_keep_empty_lines
	and set -l _flag_K $_flag_keep_empty_lines

	#template renderer (command)
	set -l cmd "command chevron"

	set -q MUSTACHE_PARTIALS_PATH
	and test -n "$MUSTACHE_PARTIALS_PATH"
	and set -a cmd "-p" (builtin string join -- ' -p ' $MUSTACHE_PARTIALS_PATH)

	#data source
	if not set -q _flag_d
		set -l _flag_d (dirname $argv[1])/(basename $argv[1] '.mustache')'.json'
		not test -f "$_flag_d"
		fish_error "No data set provided nor found"
		exit 1
	end
	set -a cmd "-d (sed -e 's/'\t'/  /g' \"$_flag_d\" | psub)"

	#template
 	set -la cmd "$argv"

	#drop empty lines
	set -q _flag_K
	and contains -- (builtin string lower -- $_flag_K) no false 0
	and set -e _flag_K
	not set -q _flag_K
	and set -la cmd '| sed \'/^\s*$/d\''

	#write to file instead of stdout
	set -q _flag_o
	and set -la cmd '>"$_flag_o"'

	#execute
	fish_debug "evaluating: $cmd"
	eval $cmd
end
alias µ mustache
