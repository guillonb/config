function highlight
	argparse -i 'Y-highlight=' -- $argv

	set -q _flag_highlight
	and eval set_color $_flag_highlight >/dev/null
	and set _flag_highlight (\
	eval set_color $_flag_highlight |\
	builtin string escape |\
	sed -e 's/\\\\e\\\\\[\([0-9]\+\)m/\1 /g' |\
	builtin string split -- ' ' |\
	builtin string join -- ';' |\
	builtin string trim --chars=';')

	set -l eoo
	contains -- '--' $argv
	and set eoo (contains -i -- '--' $argv)
	or set eoo -1
	contains -- '-e' $argv[1..$eoo]
	or contains -- '-f' $argv[1..$eoo]
	or echo $argv[1..$eoo] | grep -q '\s--file=\|\s--regexp='
	and set -l hasef

	if set -q hasef
		fish_debug "env GREP_COLOR=$_flag_highlight grep --color=always -e '\$' -e $argv"
		env GREP_COLOR=$_flag_highlight grep --color=always -e '$' $argv
	else
		#work-around, PATTERN to find should be first argument
		fish_debug "env GREP_COLOR=$_flag_highlight grep --color=always '\$\|'$argv"
		eval "env GREP_COLOR='$_flag_highlight' grep --color=always '\$\|'$argv"
	end
end
