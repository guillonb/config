function tee --wraps (which tee) --description "tee wrapper that allows teeing to output tty"
	argparse -n "tee wrapper" -i 'h/help' 'o/stdout' -- $argv
	set -q _flag_h
	and command tee --help
	and echo
	and echo "-----"
	and echo "wrapper additional:"
	and echo
	and echo "  -o --stdout    write stdin to terminal output tty.  An equivalent"
	and echo "          behavior could be obtained with:"
	and echo "          \$ command tee [options] [paths] (realpath /proc/\$fish_pid/fd/1)"
	and echo
	and return 0
	
	set -q _flag_o
	and set -a argv (realpath /proc/$fish_pid/fd/1)

	command tee $argv
end
