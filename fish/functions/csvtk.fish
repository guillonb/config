function csvtk --wraps (which csvtk) --description 'csvtk wrapper for setting in and out default delimiter'
	set -l options
	set -q _csvtk_in_delimiter
	and set -a -- options '-d'$_csvtk_in_delimiter
	set -q _csvtk_out_delimiter
	and set -a -- options '-D'$_csvtk_out_delimiter
	command csvtk $options $argv
end
