function __fish_csvtk.py_complete
	set -x _ARGCOMPLETE 1
	set -x _ARGCOMPLETE_IFS \n
	set -x _ARGCOMPLETE_SUPPRESS_SPACE 1
	set -x _ARGCOMPLETE_SHELL fish
	set -x COMP_LINE (commandline -p)
	set -x COMP_POINT (builtin string length (commandline -cp))
	set -x COMP_TYPE
	if set -q _ARC_DEBUG
		python3 $HOME/.config/fish/completions/csvtk.py 8>&1 9>&2 1>/dev/null 2>&1
	else
		python3 $HOME/.config/fish/completions/csvtk.py 8>&1 9>&2 1>&9 2>&1
	end
end

complete -c csvtk -f -a '(__fish_csvtk.py_complete)'
