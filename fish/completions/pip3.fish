if functions -q pip3
	#fix of /usr/share/fish/completions/pip3.fish when pip3 has been shadowed by an alias to "/usr/bin/python3 -m pip"
	pip3 completion --fish 2>/dev/null | string replace -r -- '-c\s+.*$' '-c pip3' | source
else
	source /usr/share/fish/completions/pip3.fish
end
