#no file completion
complete -x -c cipher -n "filter -cL1" -a ""

# options
# !todo!
complete -x -c cipher -d "Display help and return" -s 'h' -l 'help'
complete -x -c cipher -d "Ignore warnings" -s 'i' -l 'ignore-warnings'
complete -x -c cipher -d "Update lock" -s 'l' -l 'update-lock'
complete -x -c cipher -d "Specify alternative configuration file" -s 'c' -l 'configuration-file' -r

# mode 
set -l cipher_mode_list "cd,clean,close,go,help,h,mount,m,open,query,q,status,s,toggle,t,unmount,u,unlock,L,lock,l,list,ls"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Change directory to cipher mount point" -a 'cd'
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Lock/unlock cipher mount point according to its status" -a 'clean'
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Unmount cipher, if mounted, and go to parent of its mount point" -a 'close'
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Mount, if not, and go to cipher" -a 'go open'
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Display help and return" -a 'help'
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "List cipher" -a "list"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Lock cipher" -a "lock"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Mount cipher" -a "mount"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Quietly query cipher status" -a "query"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Show cipher status" -a "status"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Toggle mounted/unmounted cipher" -a "toggle"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Unlock cipher" -a "unlock"
complete -x -c cipher -n "filter -cL1 -f $cipher_mode_list" -d "Unmount cipher" -a "unmount"

# cipher
for line in (grep -ve '^#' -e '^\s*$' "$cipher_conf")
	set -l cipherName (echo $line | cut -d: -f1)
	set -l cipherDesc (echo $line | sed 's/'\t\t'*/'\t'/g' | cut -f4)
	complete -x -c cipher -n "filter -cL2 -f \"$cipherName\",cd,close,go,open,ls,list,h,help,--help,-h" \
		-d "$cipherDesc" -a "$cipherName"
	complete -x -c cipher -n "filter -cL2 -M2 -f \"$cipherName\",ls,list,h,help,--help,-h -a go,cd,close,open" \
		-d "$cipherDesc" -a "$cipherName"
end

