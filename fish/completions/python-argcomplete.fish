#This script is not sourced. Please consider outputing the completion in an autoloaded completions/<cmd>.fish file.

set -l 2CompCmdList pubs
for cmd in $2CompCmdList
	register-python-argcomplete --shell fish $cmd | .
end
