"below line was commented because of folding bugs (just tried).
"Next was inserted instead (c.f., FastFold Readme.md).
"How does markdown plugin behaves with Fastfold plugin?
"let g:vim_markdown_folding_disabled = 1
let g:markdown_folding = 1

let g:vim_markdown_fenced_languages = ['csharp=cs','c++=c','viml=vim','bash=sh','ini=dosini','caml=ml','tikz=tex', 'sql=sql']

"extensions
"let g:vim_markdown_math = 1
let g:vim_markdown_json_frontmatter = 1
let g:vim_markdown_strikethrough = 1

set spell
