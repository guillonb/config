
let g:tex_fold_enabled=1
let g:LatexBox_Folding=1
let g:Tex_FoldedSections = 'part|addpart,chapter|addchap,section|addsec,subsection,subsubsection,paragraph,subparagraph'
let g:Tex_FoldedEnvironments = 'abstract,align,center,comment,description,enumerate,eq,figure,flushleft,flushright,frame,gather,itemize,keywords,lst,minipage,overlayarea,scope,standaloneframe,table,tabular,thebibliography,tikz,titlepage,verbatim,questions,instructions,exercize'
let g:Tex_FoldedMisc = 'slide,preamble,<<<'

set foldmethod=expr
set iskeyword+=:
set fdm=syntax
set spell
