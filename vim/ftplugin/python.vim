
let g:SimpylFold_docstring_preview=1
let g:SimpylFold_fold_docstring=1
let g:SimpylFold_fold_import=1

set tabstop=2
set softtabstop=2
set shiftwidth=2
set noexpandtab

set spell
hi SpellBad ctermbg=white

